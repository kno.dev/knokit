#!/bin/sh
# This sets up the build environment and also determines which packages will be needed at runtime

export BS=etc/build/os/redhat;
export RS=etc/setup;
export TRACE_SETUP=${TRACE_SETUP:-false};
export INSTALL_OUTPUT=/dev/null
export JUST_VALIDATE=false;
export OPTDIR DNF_OPTS;
if [ -d options ]; then
    OPTDIR=options;
elif [ -d ../../../../options ]; then
    OPTDIR=../../../../options;
fi;

# Helpful things to put in options/dfnopts:
#  --enablerepo=xxx
#  --setopt=metadata_expire=3600
#  --setopt=*.metadata_expire=3600
if [ -d "${OPTDIR}" ] && [ -f "${OPTDIR}/dnfopts" ]; then
    DNF_OPTS=$(cat "${OPTDIR}/dnfopts");
elif [ -d "${OPTDIR}" ] && [ -f "${OPTDIR}/yumopts" ]; then
    DNF_OPTS=$(cat "${OPTDIR}/yumopts");
fi;

DNF_OPTS="${DNF_OPTS} --skip-broken --nobest";

# This command is usually executed inside knobuild.command which has
# tried to figure out the distro and will pass that as an
# argument. The *distro* is finer grained that the *platform* (which is
# redhat, debian, alpine, etc).
if [ $# -gt 0 ]; then
    export DISTRO=$1;
    shift;
fi;

# Figure out if we can be root to do installs. If we can't we just
# validate whether the packages we would like to see are available.
if [ "${EUID}" = 0 ] ||
       ( [ -z "${EUID}" ] && [ "${UID}" = 0 ] ) ||
       [ "${HOME}" = "/root" ]; then
    if ${TRACE_SETUP}; then
        echo "# kno/build_setup: Already root, not using 'sudo'">&2;
    fi;
    export SUDO=;
elif [ -f options/sudo ]; then
    export SUDO=$(cat options/sudo);
elif [ -f options/nosudo ]; then
    export SUDO=;
    export JUST_VALIDATE=true;
else
    export SUDO=$(which sudo 2> /dev/null);
    # We could probably check here that we can actually do installs
    # with sudo, but that's a little more tricky.
    if [ -z "${SUDO}" ]; then
        echo "# kno/build_setup: No 'sudo' and not root, just validating">&2;
        JUST_VALIDATE=true;
    fi;
fi;

echo "# knob/setup: Updating repository cache with check-update" >&2;
${SUDO} dnf -y ${DNF_OPTS} check-update;
#DNF_OPTS="-C ${DNF_OPTS}";
#DNF_OPTS="--setopt=metadata_expire=3600 --setopt=*.metadata_expire=3600 ${DNF_OPTS}";
DNF="${SUDO} dnf -y ${DNF_OPTS}"
DNF_INSTALL="${DNF} install"

buildmsg() {
    if ${TRACE_SETUP}; then
        echo "# knob/build_setup:" $* >&2;
    fi;
}

# DNF_INSTALL( ) {
#     ${SUDO} ${DNF} install $* > /dev/null 2>&1 || echo "Failed to install $@";
# }
# DNFMY( ) {
#     local pkg;
#     for pkg in $@; do
#         ${SUDO} ${DNF} install ${pkg} > /dev/null 2>&1 || echo "Failed to install ${pkg}";
#     done;
# }

# Install tools

pkg_installedp()
{
    local pkg=$1;
    if rpm -qs ${pkg} >/dev/null 2>&1; then
        return 0;
    else
        return 1;
    fi;
}

pkg_availablep()
{
    local pkg=$1;
    if dnf list -q "${pkg}" >/dev/null 2>&1; then
        return 0;
    else
        return 1;
    fi;
}

dnf_try() {
    local needed="";
    local pkg=false;
    for pkg in $*; do
        if ! pkg_installedp ${pkg}; then
            needed="${needed} ${pkg}";
        fi;
    done;
    if ${JUST_VALIDATE}; then
        echo "# knodb/setup: The following packages are not installed: ${needed}";
    elif ! ${DNF_INSTALL} ${needed} >>${INSTALL_OUTPUT} 2>&1 ; then
        for pkg in $*; do
            if ! pkg_installedp ${pkg}; then
                (${DNF_INSTALL} ${pkg} >>${INSTALL_OUTPUT} 2>&1 ||
                     echo "#!! kno/build_setup: Failed to install ${pkg}");
            fi;
        done;
    fi;
}
try_install() {
    local pkg=$1;
    if ${JUST_VALIDATE}; then
        if ! pkg_installedp "${pkg}"; then
            MISSING_PACKAGES="${MISSING_PACKAGES} ${pkg}";
        fi;
    elif ! pkg_installedp ${pkg}; then
        if ${TRACE_SETUP}; then echo "# kno/build_setup: Trying to install ${pkg}" >&2; fi
        ${DNF_INSTALL} ${pkg} >${INSTALL_OUTPUT} 2>&1 || echo >/dev/null;
        if ! pkg_installedp ${pkg}; then
            MISSING_PACKAGES="${MISSING_PACKAGES} ${pkg}";
            echo "# kno/build_setup: Failed to install ${pkg}" >&2;
        fi;
    elif ${TRACE_SETUP}; then
        echo "# kno/build_setup: Package ${pkg} is already installed" >&2;
    fi;
}
try_install_packages() {
    local pkgfile=$1;
    local pkg=;
    if [ -f "${pkgfile}" ]; then
        if ${TRACE_SETUP}; then
            if ${JUST_VALIDATE}; then
                echo "# kno/build_setup: Checking packages from ${pkgfile}" >&2;
            else
                echo "# kno/build_setup: Installing packages from ${pkgfile}" >&2;
            fi;
        fi;
        grep -v "^\s*#" ${pkgfile} 2>/dev/null | while read pkg; do try_install ${pkg}; done;
    else
        echo "# kno/build_setup: Packaging file ${pkgfile} does not exist" >&2;
    fi;
}
        # for pkg in $(grep -v "^\s*#" ${pkgfile}); do
        #     try_install ${pkg};
        # done;

# Reflection tools

get_pkg_depends ()
{
    # Gets a subset of a packages dependencies to try and get the runtime
    # packages for a given development package
    local dev_pkg=$1;
    dnf deplist --installed ${dev_pkg} | grep "dependency:" | \
        grep -v rpmlib | grep -v pkgconfig | grep -v rtld | \
        grep -v '.so' | grep -v '/' | \
        sed -e 's/dependency://' -e 's/(.*$//' -e 's/\s*//' -e 's/ =.*$//' | \
        sort | uniq;
}

get_runtime_packages ()
{
    # Tries to get the runtime packages based on the development package
    # The default behavior filters the dependencies of the development package
    # But this provides special cases where the default mechanism doesn't work
    local dev_pkg=$1;
    case ${dev_pkg} in
        mongo-c-driver-devel)
            echo mongo-c-driver;
            return 0;
            ;;
        python3-devel)
            echo python3;
            echo python3-libs;
            return 0;
            ;;
        jemalloc-devel)
            echo jemalloc;
            return 0;
            ;;
        gettext-devel)
            echo gettext;
            echo gettext-libs;
            return 0;
            ;;
        # Put special cases here
        *)
            ;;
    esac;
    get_pkg_depends ${dev_pkg} 2>/dev/null | grep -v '\-dev';
}

dev_install ()
{
    # This installs a dev package for a component and adds the corresponding
    # runtime packages to the setup directory where they will be copied to the
    # application tree for dependency installation
    local component=$1; shift;
    if [ $# = 0 ]; then return 0; fi;
    rm -f ${RS}/components/${component}*;
    touch ${RS}/components/${component}-build;
    touch ${RS}/components/${component};
    if ! ${JUST_VALIDATE}; then
        ${DNF_INSTALL} $*;
    fi;
    for pkg in $*; do
        if pkg_installedp "${pkg}"; then
            echo ${pkg} >> ${RS}/components/${component}-build;
            get_runtime_packages ${pkg} >> ${RS}/components/${component};
        else
            echo "# knodb/setup: Couldn't install package '${pkg}'";
            MISSING_PACKAGES="${MISSING_PACKAGES} ${pkg}";
        fi;
    done;
}

# Mapping components into build dependencies (which will try to guess runtime dependencies)

get_requirements()
{
    local components=${1};
    for component in ${components}; do
        case ${component} in
            archivetools|kno-archivetools)
                echo libarchive-devel;
                ;;
            mariadb|kno-mariadb)
                echo mariadb-connector-c-devel;
                ;;
            mongodb|kno-mongodb)
                echo mongo-c-driver-devel;
                ;;
            postgres|kno-postgres)
                if dnf list -q libpq-devel > /dev/null 2>&1; then
                    echo libpq-devel;
                else
                    echo postgresql-devel;
                fi;
                ;;
            odbc|kno-odbc)
                echo unixODBC-devel;
                ;;
            zeromq|kno-zeromq)
                echo czmq-devel;
                ;;
            imagetools|kno-imagetools)
                echo libpng-devel libexif-devel qrencode-devel ImageMagick-devel;
                ;;
            leveldb|kno-leveldb)
                echo leveldb-devel;
                ;;
            rocksdb|kno-rocksdb)
                echo rocksdb-devel;
                ;;
            hunspell|kno-hunspell)
                echo hunspell-devel;
                ;;
            hyphenate|kno-hyphenate)
                echo hyphen-devel;
                ;;
            readstat|kno-readstat)
                echo bzip2-devel xz-devel;
                ;;
            ziptools|kno-ziptools)
                echo libzip-devel;
                ;;
        esac;
    done;
}

init_runtime_setup()
{
    cp -f ${BS}/runtime_setup ${RS}/runtime_setup;
    grep -v "^\s*#" ${BS}/runtime_packages > ${RS}/runtime_packages;
    if [ ! -d ${RS}/components ]; then mkdir ${RS}/components; fi
}

if [ $# -gt 0 ]; then
    COMPONENTS=$@;
else
    COMPONENTS="mariadb mongodb postgres odbc leveldb rocksdb \
                sundown tidy imagetools hunspell hyphenate \
                archivetools readstat ziptools \
                nng zeromq";
fi;

CORE_DEV_PACKAGES="snappy-devel csnappy-devel csnappy libffi-devel openssl-devel \
                   gettext-devel uuid-devel ldns-devel sqlite-devel libcurl-devel \
                   jemalloc-devel libzstd-devel python3-devel cmark-devel libedit-devel"

if [ -z "${DONT_SETUP}" ]; then
    if ${TRACE_SETUP}; then echo "# knob/build_setup: Preparing to build" ${COMPONENTS} >&2; fi
    ${DNF} update && buildmsg "updated" &&
        ${DNF} upgrade && buildmsg "upgraded" &&
        try_install_packages ${BS}/build_packages && buildmsg "generic build setup done" &&
        if [ -n "${MISSING_PACKAGES}" ]; then
            echo "# knodb/setup: These packages were not available: ${MISSING_PACKAGES}";
        fi;
        init_runtime_setup && buildmsg "runtime setup initialized" &&
        rm -f $RS/components/* &&
        dev_install core ${CORE_DEV_PACKAGES} && buildmsg "ready to build KNO core" &&
        (for component in ${COMPONENTS}; do
             deps=$(get_requirements ${component});
             if [ -z "${deps}" ]; then
                 if ${TRACE_SETUP}; then
                     echo "# knob/build_setup: No dependencies for ${component}";
                 fi;
             else
                 ((dev_install ${component} $(get_requirements ${component}) &&
                       buildmsg "ready to build ${component}") ||
                      echo "#! knob/build_setup: Couldn't install dependencies for ${component}: ${deps}" >&2)
             fi;
         done)
    echo libu8 kno ${COMPONENTS} > options/ready;
fi;
if [ -n "${MISSING_PACKAGES}" ]; then
    echo "# knodb/setup: Missing packages: ${MISSING_PACKAGES}";
fi;
