This directory provides tools for installing KNO build dependencies on
RedHat derived operating systems.

If your system has some repositories disabled (in
/etc/yum.repos.d/..), you can add `--enable-repo` arguments to
options/yumopts to use them in finding components.

