GTAGS_FLAGS=--skip-symlink
ETAGS=$(shell ./bin/find_etags)

default: tags/KNOBTAGS tags/ALLTAGS tags/SCHEMETAGS \
	 tags/KNOTAGS tags/KNOCTAGS tags/KNOCCTAGS

all:
	@./knob all

contribs:
	@./knob contribs

libu8: libu8/bin/u8run
libu8/bin/u8run: libu8/*.c libu8/*.h
	@./knob libu8

kno: kno/bin/knox
kno/bin/knox: libu8/bin/u8run kno/makefile kno/include/kno/*.h \
		kno/src/*/*.c kno/src/*/*.h
	@./knob kno

rebuild:
	@./knob rebuild all

compilation_commands.json: *_compilation_commands.json
	cat *_compilation_commands.json > compilation_commands.json

# TAG Targets

etags: tags/KNOBTAGS tags/SCHEMETAGS tags/ALLTAGS
	tags/KNOTAGS tags/KNOCTAGS tags/KNOCCTAGS
alltags: etags

GTAGS:
	if ! which global >/dev/null 2>&1; then 		\
	  echo "GTAGS: 'global' is not installed"; 		\
	elif [ -f GTAGS ]; then 				\
	  global -u; 						\
	elif which ctags-universal > /dev/null 2>&1; then 	\
	  gtags --gtagslabel universal-ctags ${GTAGS_FLAGS}; 	\
	elif which ctags-exuberant > /dev/null 2>&1; then 	\
	  gtags --gtagslabel exuberant-ctags ${GTAGS_FLAGS}; 	\
	else 							\
	  gtags ${GTAGS_FLAGS}; 				\
	fi;

tags/KNOBTAGS: makefile knobuild.command common.sh README.md \
		bin/build.libu8 bin/build.kno \
		bin/kno_activate bin/kno_exec bin/exec_env.template \
		bin/pull bin/refresh bin/setup bin/update bin/clean \
		bin/git.check bin/git.toplog bin/git.sum \
		bin/setroot bin/maketar bin/makesourcetar \
		bin/gitversion
	@rm -f TAGS && \
	 $(ETAGS) -o $@ makefile common.sh knobuild.command README.md bin/*

tags/SCHEMETAGS: makefile
	@if [ -n "$(FORCE)" ] || [ ! -f $@ ] || \
		   ( find . -name "*.scm" -type f -newer $@ >> $@.changes && \
		     test -s $@.changes ); then \
	   echo "Building SCHEMETAGS"; rm -f $@; \
	   find . -name "*.scm" -type f | xargs -r -L 16 $(ETAGS) --append -o $@; \
	fi;

tags/ALLTAGS: makefile
	@if ( [ -n "$(FORCE)" ] || [ ! -f $@ ] || \
	      ( find . -name "*.[ch]" -type f -newer $@ > $@.changes && \
	        find . -name "*.scm" -type f -newer $@ >> $@.changes && \
	        test -s $@.changes ) ); then \
	   echo "Building $@"; rm -f $@; \
	   for component in libu8 kno; do \
	     find $${component} -name '*.[ch]' -type f | xargs -r -L 16 $(ETAGS) --append -o $@; \
	     find $${component} -name '*.scm' -type f | xargs -r -L 16 $(ETAGS) --append -o $@; \
	     find $${component} -name "*.in" -type f | xargs -r -L 16 $(ETAGS) --append -o $@; \
	   done; \
           for component in kno-*; do \
	     $(ETAGS) --append -o $@ $${component}/*.[ch] 2>/dev/null; \
	     find $${component}/scheme -name '*.scm' 2> /dev/null | xargs -r -L 16 $(ETAGS) --append -o $@; \
	   done; \
	   rm -f $@.changes; \
	 fi;

tags/KNOTAGS: makefile
	@if [ -n "$(FORCE)" ] || [ ! -f $@ ] || \
	   ( find kno -name "*.[ch]" -type f -newer $@ > $@.changes && \
	     find kno -name "*.scm" -type f -newer $@ >> $@.changes && \
	     test -s $@.changes ); then \
	   echo "Building $@"; rm -f $@; \
	   find kno -name "*.[ch]" -type f | xargs -r -L 16 $(ETAGS) --append -o $@; \
	   find kno -name "*.scm" -type f | xargs -r -L 16 $(ETAGS) --append -o $@; \
	   rm -f $@.changes; \
	fi;

tags/KNOCTAGS: makefile
	@if [ -n "$(FORCE)" ] || [ ! -f $@ ] || \
	    ( find kno -name "*.[ch]" -type f -newer $@ > $@.changes && test -s $@.changes ); then \
	   echo "Building $@"; rm -f $@; \
	   find kno -name "*.[ch]" -type f | xargs -r -L 16 $(ETAGS) --append -o $@; \
	   rm -f $@.changes; \
	fi;

tags/KNOCCTAGS: makefile
	@if [ -n "$(FORCE)" ] || [ ! -f $@ ] || \
	    ( find kno -name "*.c" -type f -newer $@ > $@.changes && test -s $@.changes ); then \
	   echo "Building $@"; rm -f $@; \
	   find kno -name "*.c" -type f | xargs -r -L 16 $(ETAGS) --append -o $@; \
	   rm -f $@.changes; \
	fi;

# Never can remember which one :)
cleantags tagclean tagsclean:
	rm -rf tags/*TAGS
freshtags:
	rm -rf tags/*TAGS
	make default

shellcheck:
	if which shellcheck >/dev/null 2>&1; then 			\
	  shellcheck -x knobuild.command bin/build.libu8 bin/build.kno 	\
	      bin/maketar bin/makesourcetar bin/git.* bin/clean 	\
	      bin/gitversion bin/find_etags 				\
	      bin/pull bin/refresh bin/update bin/uproot; 		\
	fi;
