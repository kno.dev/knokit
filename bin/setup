#!/bin/sh
. ./common.sh
skip_depends=false;
force_depends=false;
just_depends=false;
if [ $# -gt 0 ]; then
    case $1 in
        skipdepends|nodepends|source|tree)
            skip_depends=true;
            shift;
            ;;
        justdepends|justos|support)
            force_depends=true;
            just_depends=true;
            shift;
            ;;
        depends|os)
            force_depends=true;
            shift;
            ;;
    esac;
fi;
if [ -f "options/skipdepends" ]; then
    skip_depends=true;
fi;
if ${skip_depends} && ! ${force_depends}; then
    echo "# knodb/setup: Skipping checking/installation of dependencies";
elif ${force_depends} || [ ! -f etc/build/build_setup_done ]; then
    done=false;
    if [ -d "etc/build/os/${PLATFORM}" ]; then
        echo "# knob: INSTALLING SOFTWARE DEPENDENCIES (may need sudo/root access)";
        "./etc/build/os/${PLATFORM}/build_setup" && done=true;
    else
        echo "# knob: Can't find OS dependency setup in" "etc/build/os/${PLATFORM}";
    fi;
    if ${done}; then
        touch etc/build/build_setup_done;
        exec "$0" skipdepends "$@";
    else
        echo "# knob: Couldn't set up OS dependencies for platform '${PLATFORM}' (DISTRO=${DISTRO})";
        echo "# knob: Available platforms are: $(ls etc/build/os)";
        echo "# knob: Do './knob set platform=.. distro=.. to override automatic settings";
        echo "# knob: Or do './knob setup (source|tree|nodepends)' to skip dependency installation";
        exit 1;
    fi;
fi;

if ${just_depends}; then
    exit;
fi;

FETCH_BRANCHES=$(getbuildopt "branches");
FETCH_BRANCHES="main edge ${FETCH_BRANCHES}";

if [ -z "${COMPONENTS}" ]; then
    if [ -f kno/contrib/ENABLED ]; then
        COMPONENTS="${KNO_BASE_COMPONENTS}";
        ENABLED=$(cat kno/contrib/ENABLED);
        for component in ${ENABLED}; do
            COMPONENTS="${COMPONENTS} kno-${component}";
        done;
    else
        msg "KNO hasn't been built, so we can't tell which extra components to build"
        msg "After you've built KNO. run './knob setup' again to identify other compoents."
        COMPONENTS="libu8 kno";
    fi;
fi;

SUMMARY=
for component in ${COMPONENTS}; do
    if disabledp "${component}" > /dev/null 2>&1; then
        echo "# knobuild/update: Skipping disabled component '${component}'";
        continue;
    fi;
    USEBRANCH=$(getbuildopt "${component}.branch" "${BRANCH}");
    if [ ! -d "${component}/.git" ]; then
        use_source=$(gitsource ${component});
        echo "Setting up ${component} using source '${use_source}'";
        if git clone --recurse-submodules -b "${USEBRANCH:-main}" "${use_source}" "${component}"; then
            ("cd" "${component}";
             for branch in ${USEBRANCH} ${FETCH_BRANCHES}; do
                 git checkout ${branch} > /dev/null ||
                     echo "# $0: Warning: Couldn't checkout ${branch} in ${component}";
             done;
             if [ -n "${USEBRANCH}" ]; then
                 git checkout ${USE_BRANCH};
             fi);
        else
            echo "# Failed: knob/update: git clone ${url} ${component}";
        fi;
    fi;
    if [ -d "${component}/.git" ]; then
        ("cd" "${component}" > /dev/null || exit;
         git fetch;
         current=$(git symbolic-ref -q HEAD);
         current="${current##refs/heads/}";
         if [ -n "${USEBRANCH}" ] && [ "${current}" != "${USEBRANCH}" ]; then
             echo "Using ${USEBRANCH} for ${component}";
             gitsetbranch "${USEBRANCH}";
             current=$(git symbolic-ref -q HEAD);
             current="${current##refs/heads/}";
         fi;
         if [ -z "${SUMMARY}" ]; then
             SUMMARY="${component}#${current}";
         else
             SUMMARY="${SUMMARY}­ ${component}#${current}";
         fi;
         GIT_OUTPUT=$(git pull --ff-only);
         if [ "${GIT_OUTPUT}" != "Already up to date." ]; then
             echo "${GIT_OUTPUT}";
         fi;
         GIT_OUTPUT="";
         if [ -f .devmodules ]; then
             if which git-devmodules >/dev/null 2>&1; then
                 git devmodules setup;
             elif test -x etc/git-devmodules; then
                 ./etc/git-devmodules init;
                 ./etc/git-devmodules list;
             else
                 echo "# Warning: Couldn't process devmodules for ${component}";
             fi;
         fi;
         if [ -f ".gitlfs" ]; then
             if [ ! -d ".git/lfs" ]; then
                 git lfs install;
             fi;
         fi);
    else
        echo "# knob/update: No component directory ${component} in $(pwd)";
    fi;
done;
if [ -n "${KNOROOT}" ]; then
   setup_knoroot ${KNOROOT};
fi;
