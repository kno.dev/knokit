#!/bin/sh
# shellcheck disable=SC2086
set -e
if [ -n "${KNOBUILD_COMMON_SH}" ]; then
    :;
elif [ -f ./common.sh ]; then
    . ./common.sh
else
    echo "Can't find common.sh from $(pwd)";
fi;

component=kno;
IS_DEPENDENCY=true;

# if [ ! -f logs/libu8.install.done ]; then
#     errmsg "Can't build KNO because libu8 hasn't been built and installed";
#     exit;
# fi;

USEBRANCH="$(getbuildopt "kno.branch" "${BRANCH}")";
USECOMMIT="$(getbuildopt "kno.commit")";
if [ -n "${USECOMMIT}" ]; then
    SHOWBRANCH="#${USECOMMIT}";
elif [ -n "${USEBRANCH}" ]; then
    SHOWBRANCH="#${USEBRANCH}";
else
    SHOWBRANCH=;
fi

export ignore_tests=false;
if ${IGNORE_TESTS} || testbuildopt kno.ignoretests || testbuildopt kno.reckless; then
    export ignore_tests=true;
fi;

KNO_CONFIG_ARGS=$(getbuildopt kno.configs);
KNO_CONFIG_ARGS="$(getbuildopt kno.configs)";
if [ -n "${KNO_CONFIG_ARGS}" ]; then
    CONFIG_ARGS="${CONFIG_ARGS} ${KNO_CONFIG_ARGS}";
fi;

if testbuildopt malloc; then
    CONFIG_ARGS="${CONFIG_ARGS} --with-malloc=$(getbuildopt malloc)";
fi

if testbuildopt stdmods; then
    comma_list=;
    for mod in $(getbuildopt stdmods); do
        if [ -z "${comma_list}" ]; then
            comma_list="${mod}";
        else
            comma_list="${comma_list},${mod}";
        fi;
    done;
    CONFIG_ARGS="${CONFIG_ARGS} --with-standard-modules=${comma_list}";
fi

if testbuildopt optlevel; then
    CONFIG_ARGS="${CONFIG_ARGS} OPTLEVEL=$(getbuildopt optlevel)";
fi;

if testbuildopt sandbox; then
    CONFIG_ARGS="${CONFIG_ARGS} SANDBOX=$(getbuildopt sandbox)";
fi;

if testbuildopt offline; then
    CONFIG_ARGS="${CONFIG_ARGS} OFFLINE=$(getbuildopt offline)";
fi;

if testbuildopt sandbox; then
    CONFIG_ARGS="${CONFIG_ARGS} SANDBOX=$(getbuildopt sandbox)";
fi;

if testbuildopt devmode; then
    KNOSRC="$(pwd)/kno";
    CONFIG_ARGS="${CONFIG_ARGS} --with-alt-libscm=buildtree --with-stdlibs=buildtree";
    MAKE_FLAGS="DEVMODE=true ${MAKE_FLAGS}";
fi;

if [ -n "${SKIP_FASLS}" ]; then
    MAKE_FLAGS="SKIP_FASL=yes ${MAKE_FLAGS}";
elif [ -f "$(pwd)/kno/makefile.in" ] &&
         grep "fasl:" "$(pwd)/kno/makefile.in" > /dev/null 2>&1; then
    BUILD_FASLS=true;
else
    BUILD_FASLS=false;
fi

export ENV_ARGS=$(get_env_args kno);

if [ -f "$(pwd)/kno/makefile.in" ] &&
       grep libscm-fasl "$(pwd)/kno/makefile.in" > /dev/null 2>&1; then
    DEFAULT_FASL=full;
else
    DEFAULT_FASL=none;
fi;

if testbuildopt fasl; then
    KNOSRC="$(pwd)/kno";
    FASL_CONFIG=$(getbuildopt fasl)
    if [ "${DEFAULT_FASL}" = "none" ]; then
        echo "# Ignoring FASL config (${FASL_CONFIG}) because the KNO version won't support it";
        FASL_CONFIG=;
    else case $(getbuildopt fasl) in
             none|no)
                 FASL_CONFIG=;
                 ;;
             full|yes)
                 FASL_CONFIG="yes";
                 ;;
             optional|libscm|experimental|preferred)
                 FASL_CONFIG=$(getbuildopt fasl);
                 ;;
             *)
                 echo "##!!! Unknown FASL config ${FASL_CONFIG}, ignoring";
                 FASL_CONFIG=${DEFAULT_FASL};
         esac;
    fi;
fi;

export DEFAULT_TARGET=update;
if grep 'runtime:' kno/makefile.in >/dev/null 2>&1; then
    DEFAULT_TARGET=runtime;
fi;

if [ -n "${FASL_CONFIG}" ]; then
    #echo "## Using FASL config ${FASL_CONFIG}"
    CONFIG_ARGS="${CONFIG_ARGS} --enable-fasl=${FASL_CONFIG}";
elif [ -n "${DEFAULT_FASL}" ] && [ "${DEFAULT_FASL}" != "none" ]; then
    CONFIG_ARGS="${CONFIG_ARGS} --enable-fasl=${DEFAULT_FASL}";
fi;

if testbuildopt "kno.buildmode"; then
    BUILDMODE=$(getbuildopt "kno.buildmode");
fi;

if [ -n "${BUILDMODE}" ]; then
    case ${BUILDMODE} in
        debugging|debug)
            MAKE_TARGETS=debug;
            ;;
        fast|optimized)
            MAKE_TARGETS=fast;
            ;;
        faster)
            MAKE_TARGETS=faster;
            ;;
        *)
            echo "Unknown buildmode '${BUILDMODE}' for kno";
            MAKE_TARGETS=runtime;
            ;;
    esac;
else
    MAKE_TARGETS=runtime;
fi;

addconfig() {
    local optname=$1;
    local config_option=${2:---with-${optname}};
    if testbuildopt ${optname}; then
        CONFIG_ARGS="${CONFIG_ARGS} ${config_option}=$(getbuildopt ${optname})";
    fi;
}
addconfig logdir;
addconfig rundir;
addconfig bugjar;
addconfig sharedir --with-share-dir;
addconfig datadir --with-data-dir;

for withopt in options/with-* options/without-*; do
    case ${withopt} in
        *~)
            ;;
        *)
            config_optname="--${withopt#options/}";
            if [ ! -r "${withopt}" ]; then
                :;
            elif [ -s "${withopt}" ]; then
                CONFIG_ARGS="${CONFIG_ARGS} ${config_optname}";
            else
                CONFIG_ARGS="${CONFIG_ARGS} ${config_optname}=$(cat ${optname})";
            fi;
            ;;
    esac;
done;

CONFIG_ARGS="${CONFIG_ARGS} --with-sourcedir=$(pwd)";

if [ -n "${KNOB_CC}" ]; then
    CONFIG_ARGS="CC=${KNOB_CC} ${CONFIG_ARGS}"
fi;

if [ -n "${KNOROOT}" ]; then
    CONFIG_ARGS="--prefix=${KNOROOT} --without-sudo --with-setup=none --with-sysroot=${KNOROOT} --enable-relocation ${CONFIG_ARGS}";
elif [ -n "${PREFIX}" ]; then
    CONFIG_ARGS="--prefix=${PREFIX} ${CONFIG_ARGS}";
else
    CONFIG_ARGS="${CONFIG_ARGS}";
fi

if testbuildopt "kno.sast"; then
    CONFIG_ARGS="${CONFIG_ARGS} --enable-sast=$(getbuildopt kno.sast)";
elif testbuildopt "sast"; then
    CONFIG_ARGS="${CONFIG_ARGS} --enable-sast=$(getbuildopt sast)";
fi

if [ -f ./kno/.knob_configs ]; then
    echo ${CONFIG_ARGS} > ./kno/.new_knob_configs;
    if ! diff ./kno/.new_knob_configs ./kno/.knob_configs >/dev/null 2>&1; then
        RECONFIGURE=true;
        echo "# knob: kno configuration changed";
        cp ./kno/.new_knob_configs  ./kno/.knob_configs;
    else rm ./kno/.new_knob_configs;
    fi;
else
    echo ${CONFIG_ARGS} > ./kno/.knob_configs;
fi;

"cd" kno > /dev/null || phase_failed kno chdir;
gitcheckbranch;
${ECHO_INLINE} "# knob: building kno${SHOWBRANCH}... ";
if ${REBUILD}; then
    clean_phase "${component}";
fi;
if ${RECONFIGURE} || [ ! -f makefile ] || [ ! -f config.status ] || ( ! ${SKIP_CONFIG} && [ configure -nt makefile ] ); then
    rm -f .buildmode .malloc
    phase_start configure kno;
    (./configure ${CONFIG_ARGS} > ../logs/kno.config.log 2> ../logs/kno.config.err &&
	 phase_complete configure kno "d") ||
        { phase_failed configure kno; };
elif [ -x config.status ] && ! ${SKIP_CONFIG} && ! knob_check_templates rerun.config.status; then
    phase_start config.status kno;
    (./config.status >> ../logs/kno.config.log 2> ../logs/kno.config.err && \
         phase_complete config.status kno) ||
        { phase_failed config.status kno noexit; };
elif [ -f config.log ] &&
         ! find .  ! -readable -prune -o -name '*.in' -and -newer config.log -and -newer configure | read -r 2> /dev/null; then
    ${ECHO_INLINE} " (config unchanged)";
elif ! ${SKIP_CONFIG}; then
    ${ECHO_INLINE} " (preconfigured)";
fi;
if ! ${SKIP_MAKE}; then
    phase_start make kno
    echo "# kno make," "$(date)" > ../logs/kno.make.log;
    echo "# make ${MAKE_TARGETS}" >> ../logs/kno.make.log;
    echo "# flags: ${MAKE_FLAGS}" >> ../logs/kno.make.log;
    echo "# env args: ${ENV_ARGS}" >> ../logs/kno.make.log;
    (${BEARHUG} make -j ${ENV_ARGS} ${MAKE_FLAGS} ${MAKE_TARGETS} >> ../logs/kno.make.log  2> ../logs/kno.make.err &&
         phase_complete make kno "de") ||
        { echo; phase_failed make kno noexit; };
fi;
if ${BUILD_FASLS}; then
    if ! ${SKIP_FASLS}; then
        phase_start fasls kno;
        if make -j ${MAKE_FLAGS} fasl >> ../logs/kno.fasls.log  2>> ../logs/kno.fasls.err; then
            phase_complete fasls kno;
        elif ! ${REQUIRE_FASLS}; then
            phase_complete fasls kno " with warnings";
        else
            phase_failed fasls kno noexit;
        fi;
    else
        ${ECHO_INLINE} " (skipping fasls)";
    fi;
fi;
if ${SKIP_TESTS} || testbuildopt kno.skiptests; then
    ${ECHO_INLINE} " (skipping tests)";
else
    phase_start tests kno testing;
    if testbuildopt kno.tests; then
        TEST_TARGET=$(getbuildopt kno.tests fast_tests);
    elif grep " ci_tests " makefile >/dev/null 2>&1; then
        TEST_TARGET=ci_tests;
    elif grep " fast_tests " makefile >/dev/null 2>&1; then
        TEST_TARGET=fast_tests;
    elif grep "(^| )tests:" makefile >/dev/null 2>&1; then
        TEST_TARGET=tests;
    else TEST_TARGET=tests;
    fi;
    if ${SAVE_TESTS} || testbuildopt kno.savetests; then
        TEST_FLAGS="SAVE_TESTS=true";
    fi;
    if make ${MAKE_FLAGS} ${TEST_FLAGS} ${TEST_TARGET} > ../logs/kno.tests.log 2> ../logs/kno.tests.err; then
        if [ -f "${TEST_TARGET}.waived" ]; then
            phase_complete tests kno " waived";
        else
            phase_complete tests kno "ed";
        fi;
    elif ${ignore_tests}; then
        phase_complete tests kno " ignoring failure";
    else
        phase_failed tests kno;
    fi;
fi;
if ! ${SKIP_INSTALL}; then
    phase_start install kno;
    (make ${MAKE_FLAGS} install > ../logs/kno.install.log 2> ../logs/kno.install.err &&
         touch ../logs/kno.install.done &&
         phase_complete install kno "ed") ||
        { phase_failed install kno; };
fi;
build_done;
if [ -f contrib/ENABLED ]; then
    echo "Enabled contribs are: $(cat contrib/ENABLED)";
fi;

