# knobuild

## Getting started

This is an environment for building KNO and its component modules. The
basic idea is that the source for those components live in
subdirectories of this directory and the `knob` command can build
either individual components or sets of components. The `knob` command
must be run in this directory.

Options for `knob` live in the `options` subdirectory and can be
listed with `knob opts` and can be defined either globally, e.g.
```
./knob set branch=edge
```
or for particular components:
```
./knob set libu8.branch=edge
```

`knob` also installs KNO components and this can follows two models:
* *integrated* installs put all the files beneath a particular file
  system root (the *prefix*, e.g. `/usr`) and assume that the standard
  subdirectories (such as `bin`, `log`, `run`, or `share`) are defined
  beneath that root.
* *image* installs assume that there is a KNO specific install
  directory (the *knoroot*, e.g. `/opt/kno`) and creates all of the
  directories beneath that directory.

The installation model is based on whether the *knob option* `knoroot`
or `prefix` is specified, e.g.
```
knob set knoroot=/opt/kno.edge
```
or
```
knob set prefix=/user/local
```

Image installs may be neccessary if the user doesn't have privileges
to write to standard prefixes (like `/usr`) or when there's a desire
to have the installation be *relocatable*. An image install can be
**activated** by executing the output of the command
`*knoroot*/activate`. The easiest way to do this is to use shell
command substitutions, e.g.
```
$(*knoroot*/activate)
```

## The knobuild tree

Each KNO component is a subdirectory of the knobuild tree. Usually,
these directories are cloned GIT repositories set up by the `knob
setup` command. While `knob setup` clones specific component
directories, other directories can be added at need.

The command `knob git *args*` runs the command `git
*args*` in all of the component directories.

## Commands and modifiers to know

`knob *component*` builds the KNO component *component*. Most KNO
components are independent, but almost all the components depend on
the `kno` component which in turns depends on the `libu8`
component. `knob` doesn't automatically compute dependencies since a
user might not want to rebuild the base components (`libu8` and
`kno`).

The virtual components `all` and `contribs` indicate either all of the
available components or all of those components except for `libu8` and
`kno`.

`knob` command lines can be either:
* *builds* which specify a set of components and a set of modifiers.
* *actions* which affect the knob directory and state and often take
  components as arguments
* *commands* which affect the knob directory and state and often take
  components as arguments

`knob` build modifiers include:
skipinstall skiptests skipfasls skipconfig
configure install make test fasls clean rebuild

`knob` commands include:
* git
* pull
* loop
* maketar

`knob` actions include:
* set
* unset
* options
* maketar
* logs

## Options to know

* *knoroot* is the file directory where *image* installs are done;
* *buildmode* indicates the makefile target to use in building
  components, e.g. `debug`, `production`, `normal`, etc.
* *branch* is the branch to use by default for components;
  *component*.branch specifies the branch to use for *component*.


## Integrate with your tools



