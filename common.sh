#!/bin/sh
export PATH PREFIX KNOROOT KNO_SYSROOT KNOBINDIR KNOLIBDIR BUILD_VERSION KNO_GITURL;
export DISTRO PLATFORM ARCH BRANCH TOOL_PREFIX KNO_COMPONENTS SOURCES;
export ECHO_INLINE ECHO MAKE_FLAGS BUILDMODE BEARHUG USEBRANCH SHOWBRANCH;
export CUR_BRANCH UPTODATE USE_CONTRIBS KNOBUILD_LOCKED BUILD_STARTED;
export IGNORE_TESTS=${IGNORE_TESTS:-false};
export REBUILD=${REBUILD:-false};
export IS_DEPENDENCY=false;

if [ -z "${KNOBUILD_COMMON_SH}" ]; then

if ${DEBUG_KNOBUILD}; then
    echo "# knobuild(debug): loading common.sh into pid $$">&2;
fi;

if which u8_uptodate >/dev/null 2>&1; then
    UPTODATE=u8_uptodate;
elif [ -x libu8/scripts/u8_changes ]; then
    UPTODATE="libu8/scripts/u8_changes -q";
else
    UPTODATE=bin/uptodate;
fi;
KNOBUILD_COMMON_SH=$(date);
export KNO_BASE_COMPONENTS="libu8 kno";
export KNO_COMPONENTS="libu8 kno";
# We intentially have leading double spaces in CONTRIB_NAMES so that we can use
#  prefix/suffix shell variable selectors to look for names in the list
export CONTRIB_NAMES="  mariadb mongodb postgres odbc nng zeromq sundown tidy imagetools leveldb rocksdb hunspell hyphenate archivetools readstat ziptools  ";
KNO_GITURL=$(git remote get-url origin);
KNO_GITURL=${KNO_GITURL%knobuild*};
MACHINE_NAME=$(uname -m);
if [ -f kno/contrib/ENABLED ]; then
    for contrib in $(cat kno/contrib/ENABLED); do
        if [ -f options/${contrib}.disabled ] ||
               [ -f options/${contrib#kno-}.disabled ]; then
            continue;
        elif [ "${USE_CONTRIBS#*${contrib}}" != "${USE_CONTRIBS}" ]; then
            continue;
        else
            USE_CONTRIBS="${contrib} ${USE_CONTRIBS}";
        fi;
    done;
else
    USE_CONTRIBS=${CONTRIB_NAMES}
fi
CONTRIB_DIRS=
for contrib in ${USE_CONTRIBS}; do
    if [ ! -f options/${contrib}.disabled ] ||
           [ ! -f options/${contrib#kno-}.disabled ]; then
        if [ -d "kno-${contrib}" ]; then
            KNO_COMPONENTS="${KNO_COMPONENTS} kno-${contrib}";
            CONTRIB_DIRS="${CONTRIB_DIRS} kno-${contrib}";
        elif [ -d "${contrib}" ]; then
            KNO_COMPONENTS="${KNO_COMPONENTS} kno-${contrib}";
            CONTRIB_DIRS="${CONTRIB_DIRS} kno-${contrib}";
        fi;
    fi;
done;
for git_dir in */.git; do
    base=${git_dir%/.git};
    if [ -f "${base}/.disabled" ]; then
        :;
    elif [ -d ${git_dir} ] && ( [ -f "${base}/makefile" ] || [ -f "${base}/makefile.in" ] ); then
        component=${base#kno-};
        # We check all the git subdirectories to determine KNO components to build,
        #  excluding libu8, kno, explicit exclusions (disabled) and any contribs which
        #  would be declared by a KNO build in kno/contrib/ENABLED.
        if [ "${base%_submodule}" != "${base}" ]; then
            # Skipping convenience submodule directory
            :;
        elif [ "${base}" = "libu8" ] || [ "${base}" = "kno" ]; then
            # Skip these because they're already there (at the beginning)
            :;
        elif [ -f options/${component}.disabled ] ||
                 [ -f options/disabled/${component} ]; then
            # Skip disabled components
            :;
        elif [ "${CONTRIB_NAMES#* ${component} }" != "${CONTRIB_NAMES}" ]; then
            # Skip components which are already there
            :;
        else
            KNO_COMPONENTS="${KNO_COMPONENTS} ${base}";
        fi;
    fi;
done;
KNO_SUBMODULES=
for submod in *_submodule/.git; do
    if [ -d ${submod}/.git ]; then
        dir=${submod%/.git};
        KNO_SUBMODULES="${KNO_SUBMODULES} ${dir}";
    fi;
done;
for gitdir in */.git; do
    parent=$(dirname ${gitdir});
    if [ -L "${parent}" ]; then
        KNO_SUBMODULES="${KNO_SUBMODULES} ${parent}";
    fi;
done;

reqbuildopt( ) {
    local optname=$1;
    local optdir=./options;
    if [ ! -d ./options ] && [ -d ../options ]; then
        optdir=../options;
    fi;
    if [ -f "${optdir}/${optname}" ]; then
        cat "${optdir}/${optname}";
        return 0;
    elif [ -f "${optdir}/defaults/${optname}" ]; then
        cat "${optdir}/defaults/${optname}";
        return 0;
    elif [ $# -gt 1 ]; then
        echo "$2";
        return 0;
    else
        return 1;
    fi;
}

getbuildopt( ) {
    local optname=$1;
    local optdir=./options;
    if [ ! -d ./options ] && [ -d ../options ]; then
        optdir=../options;
    fi;
    if [ -f "${optdir}/${optname}" ]; then
        cat "${optdir}/${optname}";
        return 0;
    elif [ -f "${optdir}/defaults/${optname}" ]; then
        cat "${optdir}/defaults/${optname}";
        return 0;
    elif [ $# -gt 1 ]; then
        echo "$2";
        return 0;
    else
        return 0;
    fi;
}

getboolbuildopt( ) {
    local optname=$1;
    local optval="$(getbuildopt $*)";
    case ${optval} in
        true|false)
            ;;
        yes|on|1|ok|oui|ja)
            echo true;
            ;;
        *)
            echo false;
            ;;
    esac;
}

testbuildopt( ) {
    local optname=$1;
    local optdir=./options;
    if [ ! -d ./options ] && [ -d ../options ]; then
        optdir=../options;
    fi;
    if [ -f "${optdir}/${optname}" ]; then
        return 0;
    elif [ -f "${optdir}/defaults/${optname}" ]; then
        return 0;
    else
        return 1;
    fi;
}

dbgnotefile() {
    if ${DEBUG_KNOBUILD}; then
        if [ -f "$1" ]; then
            echo "# knob: found $(ls -l \"$1\")";
        fi;
    fi;
}

dbgmsg() {
    if ${DEBUG_KNOBUILD}; then
        echo "# (debug) knobuild:" "$@";
    fi;
}

msg() {
    if ! ${QUIET_KNOBUILD}; then
        echo "# knobuild:" "$@";
    fi;
}

vmsg() {
    if ${VERBOSE_KNOBUILD}; then
        echo "# (verbose) knobuild:" "$@";
    fi;
}

errmsg() {
    echo "# knobuild:" "$@";
}

checkpath( ) {
    local check=$1;
    local head="${2%%:*}";
    local next="${2#*:}";
    local scan="${next}";
    while [ -n "${scan}" ]; do
         if [ "${head}" = "${check}" ]; then
            return 0;
        else
            head="${scan%%:*}";
            next="${scan#*:}";
            if [ "${next}" = "${scan}" ]; then
                if [ "${next}" = "${check}" ]; then
                    return 0;
                else
                    return 1;
                fi;
            else
                scan="${next}";
            fi;
        fi;
    done;
    return 1;
}

checkpath( ) {
    local check=$1;
    local path=$2;
    echo "${path}" | grep ":${check}:|^${check}:|:${check}\$" > /dev/null;
}

setup_symlink() {
    local source=$1;
    local target=$2;
    if [ ! -L ${target} ]; then
        ln -sf ${source} ${target};
    fi;
}

giturl ( ) {
    local arg=$1;
    local reponame=${arg};
    local default_url="${KNO_GITURL}${component}.git";
    if [ "${default_url#/}" != "${default_url}" ] &&
           [ ! -d "${default_url}" ]; then
        default_url="https://gitlab.com/kno.dev/${component}.git/";
    fi;
    local component=${arg#kno-};
    local url=$(getbuildopt "${component}.url" "${default_url}");
    if [ -z "${url}" ] && [ -n "${default_url}" ]; then
        echo ${default_url};
    elif [ "${url#../}" != "${url}" ]; then
        echo echo ${gitroot}${url#../};
    else
        echo ${url};
    fi;
}

gitsource ( ) {
    local component=$1;
    if [ "${GITLOCAL}" != "none" ] || [ "${GITLOCAL}" != "no" ]; then
        if [ -n "${GITLOCAL}" ] && [ -d "${GITLOCAL}/${component}/.git" ]; then
            echo "${GITLOCAL}/${component}/";
            return 0;
        elif [ -d "./gits/${component}/.git" ]; then
            echo "./gits/${component}/";
            return 0;
        elif [ -d "/gits/${component}/.git" ]; then
            echo "/gits/${component}/";
            return 0;
        fi;
    fi;
    giturl "${component}";
}

gituptodatep ( ) {
    local component=$1;
    if [ -d "${component}" ]; then
        (if command cd ${component} >/dev/null 2>&1; then
             git fetch >/dev/null 2>&1;
             if git status -uno | grep "up to date" >/dev/null 2>&1; then
                 return 0;
             else
                 return 1;
             fi;
         else
             return 1;
         fi)
    else
        return 1;
    fi;
}

gitsetbranch ( ) {
    local branch=$1;
    if [ ! -f .gitkeepbranch ]; then
        git checkout "${branch}" > /dev/null 2>&1 || echo "# Failed checkout '${branch}' for '${component}'";
        if [ -f .gitmodules ] && [ ! -d .git/modules ]; then
            git submodule init; git submodule update --recursive --init;
        fi;
        if [ -f .devmodules ]; then
            if which git-devmodules >/dev/null 2>&1; then
                git devmodules update;
            elif [ -x ./etc/git-devmodules ]; then
                ./etc/git-devmodules update;
            else
                echo "# knob: Can't update devmodules in $(pwd)";
            fi;
        fi;
    fi;
}

gitcheckbranch ( ) {
    local current=$(git symbolic-ref -q HEAD);
    local outfile;
    current="${current##refs/heads/}";
    if [ "${USEBRANCH}" = "current" ]; then
        SHOWBRANCH="#${current}";
    elif [ -n "${USEBRANCH}" ] && [ "${current}" != "${USEBRANCH}" ]; then
        echo "# knob: Switching to branch ${USEBRANCH} from '${current}' in $(pwd)";
        export RECONFIGURE=true;
        # Delete all test done files when we change branches
        rm -f *tests.done;
        outfile=$(mktemp)
        if ! git checkout ${USEBRANCH} > ${outfile} 2>&1; then
            echo "knob: Failed to checkout branch '${USEBRANCH}' in $(pwd)";
            cat ${tempfile};
            rm -f ${outfile};
            return 1;
        else
            rm -f ${outfile};
        fi;
    fi;
}

getgitbranch ( ) {
    local branch_name=$(git symbolic-ref -q HEAD);
    echo ${branch_name##refs/heads/};
}

disabledp ( ) {
    local component=${1}
    local dir=${component};
    if [ ! -d "${dir}" ] && [ ! -d "kno-${component}" ]; then
        dir="kno-${component}";
    fi;
    component="${dir#kno-}";
    if [ -f "options/${component}.disabled" ]; then
        return 0;
    elif [ -f "options/${dir}.disabled" ]; then
        return 0;
    elif [ -f "options/disabled/${component}" ]; then
        return 0;
    elif [ -f "options/disabled/${dir}" ]; then
        return 0;
    else
        return 1;
    fi;
}

generic_clean ( ) {
    find .  -name '*~'  -not -writable -exec rm {} \; 2>/dev/null || :;
    find .  -name '#*#'  -not -writable -exec rm {} \; 2>/dev/null || :;
    for suffix in .o .so .dylib .fasl .lo; do
        find . -name "*${suffix}" -writable -exec rm {} \; 2>/dev/null || :;
    done;
    return 0;
}

doclean ( ) {
    rm -f .buildmode .malloc x_compile_commands.json;
    if [ -f "makefile" ]; then
        make distclean > /dev/null 2>&1 ||
            make clean > /dev/null 2>&1 ||
            ( echo "knob: make clean failed in $(pwd)" >&2 &&
                  generic_clean 2>/dev/null );
    else generic_clean;
    fi;
}

clean_dir ( ) {
    local dir=$1;
    ( "cd" ${dir} || return 1; doclean );
}

clean_component ( ) {
    local component=${1#kno-};
    local dir=${component};
    dbgmsg "Cleaning component ${component} in ${dir}";
    if [ ! -d "${dir}" ]; then
        if [ -d "kno-${dir}" ]; then
            dir="kno-${dir}"
        else
            errmsg "knob: Can't find directory for component '${component}'";
            return 1;
        fi;
    fi;
    ( "cd" ${dir} > /dev/null || return 1; doclean; );
    if [ -x "bin/${component}.clean" ]; then
        if bin/${component}.clean; then
            ( "cd" ${dir} || return 1; doclean );
        else
            errmsg "bin/${component}.clean" failed;
            echo "knob: skipping generic clean of '${dir}'";
        fi;
    fi;
}

copy_script ( ) {
    local from=$1;
    local to=$2;
    if [ -d "${to}" ]; then
        to="${to}/$(basename ${from})";
    fi;
    # echo "Running copy_script ${from} ${to}";
    if [ ! -x "${to}" ]; then
        echo "# KNOB: Installing '${to}'";
        cp -p "${from}" "${to}";
    elif [ "${from}" -nt "${to}" ]; then
        echo "# KNOB: Updating '${to}'";
        cp -p "${from}" "${to}";
    fi;
}

setup_knoroot ( ) {
    local image=$1;
    if [ ! -d "${image}" ] && mkdir "${image}"; then
	echo "# knob: Created image root directory '${image}'";
    fi;
    if [ ! -d "${image}" ]; then
	echo "# knob: Bad image root '${image}'";
    else
        for dir in bin etc include lib share opt app log run setup; do
            if [ ! -d "${image}/${dir}" ]; then mkdir "${image}/${dir}"; fi; done
        for dir in configs modules packages; do
            if [ ! -d "${image}/${dir}" ]; then mkdir "${image}/${dir}"; fi; done
        for dir in opt/configs opt/modules opt/packages; do
            if [ ! -d "${image}/${dir}" ]; then mkdir "${image}/${dir}"; fi; done
        for dir in app/configs app/modules app/packages; do
            if [ ! -d "${image}/${dir}" ]; then mkdir "${image}/${dir}"; fi; done
        cp -a etc/setup ${image}/
        copy_script bin/kno_activate "${image}/bin/kno_activate";
        setup_symlink "bin/kno_activate" "${image}/activate";
        setup_symlink "bin/kno_activate" "${image}/";
        copy_script bin/kno_exec "${image}/bin/kno_exec";
        setup_symlink "bin/kno_exec" "${image}/";
        if [ ! -f "${image}/bin/exec_env" ]; then
            cp bin/exec_env.template "${image}/bin/exec_env";
            KNOROOT_CHANGED=true;
        fi;
        copy_script bin/knoroot "${image}/bin";
    fi;
}

build_secs ( ) {
    if [ -n "${BUILD_STARTED}" ]; then
        NOW=$(date +%s);
        SECS=$((${NOW}-${BUILD_STARTED}));
        printf " %ds" ${SECS};
    fi;
}

build_done ( ) {
    if [ -n "${BUILD_STARTED}" ]; then
        NOW=$(date +%s);
        SECS=$((${NOW}-${BUILD_STARTED}));
        printf " (finished in %ds)\n" ${SECS};
    else
        echo " (finished)";
    fi;
}

export PHASE_START CURRENT_PHASE CURRENT_COMPONENT;

phase_start()
{
    local phase=$1;
    local component=$2;
    local phase_name=${3:-${phase}};
    rm -f ../logs/${component}.${phase}.*;
    PHASE_START=$(date +%s);
    CURRENT_PHASE=${phase};
    CURRENT_COMPONENT=${component};
    ${ECHO_INLINE} " (${phase_name}...";
}

get_phase_secs() {
    local now=$(date +%s);
    if [ -n "${PHASE_START}" ]; then
        SECS=$((${now}-${PHASE_START}));
        echo ${SECS};
    fi;
}

phase_complete()
{
    local phase=$1;
    local component=$2;
    local onsuccess=$3;
    local secs=$(get_phase_secs);
    if [ -n "${onsuccess}" ]; then
        ${ECHO_INLINE} "${onsuccess}";
    else
        ${ECHO_INLINE} "";
    fi;
    if [ -s "../logs/${component}.${phase}.err" ]; then
        ${ECHO_INLINE} " with warnings";
        NOTES="${NOTES} logs/${component}.${phase}.err";
    fi;
    if [ -n "${secs}" ] && [ "${secs}" -gt 10 ]; then
        ${ECHO_INLINE} " ${secs}s)";
    elif [ -z "${onsuccess}" ]; then
        ${ECHO_INLINE} " ok)";
    else
        ${ECHO_INLINE} ")";
    fi;
    PHASE_START="";
    CURRENT_PHASE="";
    CURRENT_COMPONENT="";
}
phase_failed()
{
    local phase=$1;
    local component=$2;
    local for_exit=$3;
    shift; shift;
    local secs=$(get_phase_secs);
    if [ -n "${secs}" ]; then
        ${ECHO} " failed after ${secs}s)";
    else
        ${ECHO} " failed)";
    fi;
    echo "# knob: failed ${phase} for ${component}" $@;
    CURRENT_PHASE=;
    CURRENT_COMPONENT=;
    if ${IS_DEPENDENCY}; then
        echo "Writing failure to ../abort_knobuild from $(pwd)" >&2;
        echo "failed ${phase} for ${component}" > ../abort_knobuild;
    fi;
    if [ -z "${for_exit}" ]; then
        exit;
    elif [ "${for_exit}" != "noexit" ]; then
        ${for_exit};
    fi;
}

clean_phase ( ) {
    local component=${1#kno-};
    phase_start cleaning ${component};
    if doclean; then
        if [ -x ../bin/clean.${component} ]; then
            if ! ../bin/clean.${component} "."; then
                phase_failed cleaning ${component} " failed";
                return 1;
            else
                phase_complete cleaning ${component} "ed";
                return 0;
            fi;
        else
            phase_complete cleaning ${component} "ed";
            return 0;
        fi;
    else
        phase_failed cleaning ${component} " failed";
        return 1;
    fi;
}

get_env_args()
{
    local component=$1;
    local overrides=$2;
    local cc=$(getbuildopt ${component}.cc);
    local cflags=$(getbuildopt ${component}.cflags);
    local ldflags=$(getbuildopt ${component}.ldflags);
    if [ -n "${cc}" ]; then overrides="CC=${cc} ${overrides}"; fi;
    if [ -n "${cflags}" ]; then overrides="'KNOB_CFLAGS=${cflags}' ${overrides}"; fi;
    if [ -n "${ldflags}" ]; then overrides="'KNOB_LDFLAGS=${ldflags}' ${overrides}"; fi;
    echo ${overrides};
}

# Git custom stuff

HAVE_GITLFS=false;
if which git-lfs >/dev/null 2>&1; then
    HAVE_GITLFS=true;
fi;
if ${HAVE_GITLFS}; then
    if [ -f "./options/gitlfs" ]; then
        USE_GITLFS=$(cat "./options/gitlfs");
        case ${USE_GITLFS} in
            true|false)
                ;;
            yes|on|1)
                USE_GITLFS=true;
                ;;
            *)
                USE_GITLFS=false;
                ;;
        esac;
    else
        USE_GITLFS=true;
    fi;
else
    USE_GITLFS=false;
fi;

KNOB_CC=
if testbuildopt cc; then
    KNOB_CC=$(getbuildopt cc);
fi;
if testbuildopt cflags; then
    KNOB_CFLAGS=$(getbuildopt cflags);
fi;
if testbuildopt ldflags; then
    KNOB_LDFLAGS=$(getbuildopt ldflags);
fi;

ECHO=$(which echo 2>/dev/null || echo echo);
if [ -x "${ECHO}" ]; then
    :;
elif [ -x "/bin/echo" ]; then
    ECHO=/bin/echo;
else
    ECHO=echo;
fi
ECHO_INLINE="${ECHO} -n";

BEARHUG=
if testbuildopt "nocompiledb" || testbuildopt "nobear"; then
    BEARHUG=;
elif which bear > /dev/null 2>&1; then
    BEARHUG="bear --append --output x_compile_commands.json -- ";
fi

if [ -n "${TOOL_PREFIX}" ]; then
    :;
elif which autoconf > /dev/null 2>&1; then
    TOOL_PREFIX=$(dirname $(which autoconf));
    TOOL_PREFIX=${TOOL_PREFIX%/bin};
else
    echo "# No autoconf, can't build KNO";
    exit;
fi;

if [ -n "${LIBNAME}" ]; then
    :;
elif [ -d "${TOOL_PREFIX}/lib64" ]; then
    LIBNAME=lib64
else
    LIBNAME=lib
fi;

if [ "${TOOL_PREFIX}" != "/usr" ] && [ -d "${TOOL_PREFIX}/include" ]; then
    export CFLAGS="-I${TOOL_PREFIX}/include ${CFLAGS}";
fi;

if [ "${TOOL_PREFIX}" != "/usr" ] && [ -d "${TOOL_PREFIX}/${LIBNAME}" ]; then
    export LDFLAGS="-L${TOOL_PREFIX}/${LIBNAME} ${LDFLAGS}";
fi;

if testbuildopt distro; then
    DISTRO=$(getbuildopt distro)
else
    DISTRO=$(uname -o 2>/dev/null || uname -s)
    case "${DISTRO}" in
        GNU/Linux)
            if which lsb_release > /dev/null 2>&1; then
                distributor=$(lsb_release -s -i);
                release=$(lsb_release -s -r);
                DISTRO="${distributor}_${release}";
            fi;
            ;;
        *)
            ;;
    esac;
fi;
if [ -n "${DISTRO}" ]; then
    case "${DISTRO}" in
        *RedHat*|*redhat*|*centos*|*Fedora*|*fedora*|*FC*)
            PLATFORM=redhat;
            ;;
        *debian*|*ubuntu*|*Ubuntu*|*Mint*|*mint*)
            PLATFORM=debian;
            ;;
        *alpine*|*Alpine*)
            PLATFORM=alpine;
            ;;
        *alpine*|*Alpine*)
            PLATFORM=alpine;
            ;;
        Darwin)
            port_command="$(which port)";
            if [ -z "${port_command}" ]; then
                PLATFORM=Darwin;
            elif [ "${port_command}" = "/opt/local/bin/port" ]; then
                PLATFORM=macports;
                # Add some other tests (homebrew/fink/specific BSDs) below here
            else
                PLATFORM=macports;
            fi;
            ;;
        BSD|DragonFly)
            PLATFORM=BSD;
            ;;
    esac;
fi;

if testbuildopt arch; then
    ARCH=$(getbuildopt arch)
else
    ARCH=$(uname -m);
fi;

if [ -n "${SOURCES}" ]; then
     :;
elif testbuildopt repos; then
    SOURCES=$(getbuildopt sources);
elif [ -d /sources ] && [ -d /sources/libu8 ]; then
    SOURCES=/sources;
fi;

if [ -z "${BRANCH}" ]; then
    BRANCH=$(getbuildopt branch || true);
fi;
if [ -z "${BRANCH}" ] && [ -n "${KNO_BRANCH}" ]; then
    export BRANCH=${KNO_BRANCH};
fi;

if [ -n "${BUILD_VERSION}" ]; then
    :;
elif testbuildopt version; then
    BUILD_VERSION=$(getbuildopt version);
elif [ -f ./kno/VERSION ]; then
    BUILD_VERSION=$(cat ./kno/VERSION);
elif [ -d ./kno ]; then
    BUILD_VERSION=$("cd" ./kno > /dev/null || exit; ../bin/gitversion etc/base_version);
    echo "${BUILD_VERSION}" > ./kno/VERSION;
fi;

if [ -n "${PREFIX}" ]; then
    :;
elif testbuildopt prefix > /dev/null 2>&1; then
    PREFIX=$(getbuildopt prefix);
else
    PREFIX=;
fi;

if [ -n "${BUILDMODE}" ]; then
    :;
elif testbuildopt buildmode; then
    BUILDMODE=$(getbuildopt buildmode);
fi;

if [ -f .knoroot ]; then
    real_root=$(cat .knoroot);
    if [ -n "${KNOROOT}" ] && [ "${KNOROOT}" != "${real_root}" ]; then
        echo "!! knob: Overriding KNOROOT (${KNOROOT}) from your environment with ${real_root}";
        echo "!! knob:      '"'$(./knob activate)'"' to activate the correct root";
    fi;
    export KNOROOT=${real_root};
elif [ -n "${KNOROOT}" ]; then
    :;
elif testbuildopt knoroot > /dev/null 2>&1; then
    KNOROOT=$(getbuildopt knoroot);
elif [ -f "options/locked" ]; then
    :;
elif [ -n "${PREFIX}" ]; then
    # If we don't have an explicit knoroot but we do have a prefix,
    #  then we're doing a prefix install
    :;
elif [ -n "${BUILD_VERSION}" ]; then
    # Retrieve or generate an image root
    OPTROOT=$(getbuildopt optroot '/opt');
    KNOROOT="${OPTROOT}/kno";
    if [ -n "${BUILD_VERSION}" ]; then
        KNOROOT="${KNOROOT}-${BUILD_VERSION}";
    fi;
    if [ -n "${DISTRO}" ]; then
        KNOROOT="${KNOROOT}-${DISTRO}";
    fi;
    if [ -n "${MACHINE_NAME}" ]; then
        KNOROOT="${KNOROOT}_${MACHINE_NAME}";
    fi;
else
    :;
fi;
KNO_SYSROOT=${KNOROOT};

if [ -n "${KNOROOT}" ]; then
    PREFIX=$(getbuildopt prefix "${KNOROOT}");
fi;

KNOROOT_CHANGED=false;

# Make sure that KNOROOT is set up appropriately
if [ -n "${KNOROOT}" ]; then
    if [ ! -d "${KNOROOT}" ]; then
	if mkdir "${KNOROOT}"; then
            echo "# knob: Created '${KNOROOT}'";
	else
            echo "# knob: Couldn't create '${KNOROOT}'";
            exit;
	fi;
        KNOROOT_CHANGED=true;
    fi;

    if [ ! -d "${KNOROOT}/bin" ]; then
	if mkdir "${KNOROOT}/bin"; then
            echo "# knob: Created ${KNOROOT}/bin";
	else
            echo "# knob: Couldn't create ${KNOROOT}/bin";
            exit;
	fi;
        KNOROOT_CHANGED=true;
    fi;

    setup_knoroot "${KNOROOT}";
fi;

if [ -n "${KNOROOT}" ]; then
    USEROOT="${KNOROOT}";
    KNOBINDIR="${KNOROOT}/bin";
    if ! checkpath "${KNOBINDIR}" "${PATH}"; then
        export PATH="${KNOBINDIR}:${PATH}";
    fi;
elif [ -n "${PREFIX}" ]; then
    USEROOT="${PREFIX}";
    KNOBINDIR="${PREFIX}/bin";
    if ! checkpath "${KNOBINDIR}" "${PATH}"; then
        export PATH="${KNOBINDIR}:${PATH}";
    fi;
fi;

if [ -z "${USEROOT}" ]; then
    :;
elif [ ! -d "${USEROOT}" ]; then
    echo "knob: Invalid image root '${USEROOT}'}";
elif [ -f .knoroot ]; then
    cur=$(cat .knoroot);
    if [ "${cur}" != "${USEROOT}" ]; then
        echo "# knob: KNOROOT/PREFIX changed from ${cur} to '${USEROOT}', cleaning builds";
        for component in ${KNO_COMPONENTS}; do
            ("cd" "${component}" > /dev/null || exit; doclean >> ../logs/cleanup.log 2>&1)
        done;
        echo "${USEROOT}" > .knoroot;
        export KNOROOT_CHANGED=true;
    fi;
else
    echo "${USEROOT}" > .knoroot;
fi;

if [ -f "options/sudo" ]; then
    :;
elif [ -n "${KNOROOT}" ] || [ -f "options/nosudo" ]; then
    MAKE_FLAGS="${MAKE_FLAGS} SUDO=";
fi;

MAKE_FLAGS="${MAKE_FLAGS} U8LIBINSTALL=$(which u8_install_version 2> /dev/null||echo u8_install_version)";

export MAKE_FLAGS="${MAKE_FLAGS} PATH=${PATH}";

KNOBUILD_DIR=$(pwd)/bin
if ! checkpath "${KNOBUILD_DIR}" "${PATH}"; then
    export PATH="${KNOBUILD_DIR}:${PATH}";
fi;

if [ -f ./knobuild_custom.sh ] && [ -z "${KNOBUILD_CUSTOM_SH}" ]; then
    KNOBUILD_CUSTOM_SH=true;
    . ./knobuild_custom.sh;
fi

fi; # [ -z "${KNOBUILD_COMMON_SH}" ]

