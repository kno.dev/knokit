#!/bin/sh
# shellcheck disable=SC2086,SC2068,SC3043
set -e
export QUIET_KNOBUILD=${QUIET_KNOBUILD:-false};
export DEBUG_KNOBUILD=${DEBUG_KNOBUILD:-false};
export VERBOSE_KNOBUILD=${VERBOSE_KNOBUILD:-false};
if ${DEBUG_KNOBUILD}; then
    export VERBOSE_KNOBUILD=true;
fi;
usage () {
    echo "Usage: $0 [+rebuild] [ command | modifiers ]+ [ components | env=val | --config-option* | make_target ]";
    echo "            Builds all or some components using the specified ";
    echo "              environment variables, make targets, and config options.";
    echo "            *components* are either subdirectories of knobuild or virtual components";
    echo "              including 'all' or 'contribs'.";
    echo "            *modifiers* restricts the build to particular phases of the build process, e.g. "
    echo "              configure, clean, make, install, test"
    echo "       $0 compile: rebuild runtime";
    echo "       $0 make: rebuild runtime and fasls";
    echo "       $0 set *optname*=*value*: modifies a build option, e.g. branch=main knoroot=/opt/mykno ...";
    echo "       $0 set *optname*=*value*: modifies a build option, e.g. branch=main knoroot=/opt/mykno ...";
    echo "       $0 unset *optname*: clears a build option";
    echo "       $0 opts: Displays current build options";
    echo "       $0 log [ component | phase ]*: displays the logs of various build phases";
    echo "       $0 activate: Outputs shell commands to activate the target root image in an environment";
    echo "             Typical usage is \$(knob activate) which integrates the changes to the current environment";
    echo "       $0 help: show this message";
}
get_phaseid ( ) {
    case $1 in
        config|make|install|tests|prep|mkfasls)
            echo $1;
            return 0;
            ;;
        testing|test)
            echo tests;
            return 0;
            ;;
        conf*)
            echo config;
            return 0;
            ;;
        install*)
            echo install;
            return 0;
            ;;
        fasl|fasls|mkfasl|makefasl|makefasls)
            echo fasls;
            return 0;
            ;;
        build|compile)
            echo make;
            return 0;
            ;;
    esac;
    return 1;
}
display_logs ( ) {
    local phase=;
    local components=;
    local show_phases=;
    local just_errors=false;
    local just_list=false;
    local all_components=false;
    if [ $# -eq 0 ] || [ "$1" = "list" ]; then
        ls -l logs/*.log logs/*.err;
        return 0;
    else for spec in $@; do
             if phase=$(get_phaseid ${spec}); then
                 show_phases="${show_phases} ${phase}";
             elif [ "${spec#err}" != "${spec}" ]; then
                 just_errors=true;
             elif [ "${spec#list}" != "${spec}" ]; then
                 just_list=true;
             elif [ -d "${spec}" ]; then
                 component=${spec#kno-};
                 component=${spec#DIY/};
                 components="${components} ${spec}";
             elif [ -d "kno-${spec}" ]; then
                 components="${components} ${spec}";
             elif [ -d "DIY/${spec}" ]; then
                 components="${components} ${spec}";
             elif [ "${spec}" = "all" ]; then
                 all_components=true;
             else
                 echo "# knob: Ignoring unknown specifier '${spec}'";
             fi;
         done;
    fi;
    if ${VERBOSE_KNOBUILD}; then
        echo "Debug phases='${show_phases}' just_errors=${just_errors} components='${components}'" >&2;
    fi;
    if [ -z "${components}" ]; then
     if ! ${all_components} && [ ! -f ".component" ]; then
         all_components=true;
     else
        components="$(cat .component)";
     fi;
    fi;
    if ${all_components}; then
        for dir in *; do
            if [ -d "${dir}/.git" ]; then
                components="${components} ${dir#kno-}";
            fi;
        done;
    fi;
    for component in ${components}; do
        for phase in ${show_phases:-config make build install tests prep}; do
            if [ -s "logs/${component}.${phase}.err" ]; then
                if ${just_list}; then
                    ls -l "logs/${component}.${phase}.err";
                else
                    ( echo "■■■■ ${phase} errors for ${component} ■■■■";
                      ls -l "logs/${component}.${phase}.err";
                      cat "logs/${component}.${phase}.err" ) | more;
                fi;
            fi;
        done;
        if ! ${just_errors}; then
            for phase in ${show_phases:-config make build install tests prep}; do
                if [ -s "logs/${component}.${phase}.log" ]; then
                    if ${just_list}; then
                        ls -l "logs/${component}.${phase}.log";
                    else
                        echo "■■■■ ${phase} output for ${component} ■■■■";
                        ls -l "logs/${component}.${phase}.log";
                        cat "logs/${component}.${phase}.log";
                    fi;
                fi;
            done;
        fi;
    done;
}

# Environment setup

export ENV=
export COMPONENTS=
export SKIP_CONFIG=false;
export SKIP_MAKE=false;
export SKIP_TESTS=false;
export SKIP_FASLS=false;
export SKIP_INSTALL=false;
export IGNORE_TESTS=false;
export SAVE_TESTS=false;
export REQUIRE_FASLS=false;
export RECONFIGURE=false;
export CONFIG_ARGS=
export MAKE_TARGETS=
export MAKE_FLAGS=
export CMD_SCRIPT=
if [ ! -f common.sh ]; then
    echo "# Sorry, You need to be in the same directory as $0 to use knobuild";
    usage:
    exit;
fi;

if [ $# -eq 0 ]; then
    usage;
    if [ -f .knoroot ]; then
        echo "# The current knobuild root is '$(cat .knoroot)'";
    else
        echo "# There is no current knobuild root";
    fi;
    exit;
fi;

known_componentp() {
    local name=$1;
    case ${name} in
        kno-*)
            return 0;
            ;;
        libu8|kno)
            return 0;
            ;;
        *)
            return 1;
            ;;
    esac;
}

disabledp ( ) {
    local component=${1}
    local dir=${component};
    if [ ! -d "${dir}" ] && [ -d "kno-${component}" ]; then
        dir="kno-${component}";
    fi;
    component="${dir#kno-}";
    if [ -f "options/${component}.disabled" ]; then
        dbgnote "options/${component}.disabled";
        return 0;
    elif [ -f "options/${dir}.disabled" ]; then
        dbgnote "options/${dir}.disabled";
        return 0;
    elif [ -f "options/disabled/${component}" ]; then
        dbgnote "options/disabled/${component}";
        return 0;
    elif [ -f "options/disabled/${dir}" ]; then
        dbgnote "options/disabled/${dir}";
        return 0;
    else
        return 1;
    fi;
}

# Setting options

set_option()
{
    local optname=$1;
    local newval=$2;
    local replaced=false;
    if [ -f "options/${optname}" ]; then
	echo "# knob: Replacing current option value ${optname}='$(cat options/${optname})'";
	replaced=true;
    fi;
    if [ "${optname%%/*}" != "${optname}" ]; then
        optdir=${optname%%/*};
        if [ ! -d "options/${optdir}" ]; then
            mkdir "options/${optdir}";
        fi
    fi;

    echo "${newval}" > "options/${optname}";
    if ${replaced}; then
	echo "#     with '${newval}'";
    else
	echo "# knob: set option ${optname} to '${newval}'";
    fi;
    if [ "${optname}" = "knoroot" ]; then
        if [ ! -d "${newval}" ]; then mkdir "${newval}"; fi;
        KNOROOT=${newval}
        echo ${newval} > .knoroot;
    elif [ "${optname}" = "prefix" ]; then
        if [ -f .knoroot ]; then
            knoroot=$(cat .knoroot);
            echo "# knob: Disabling existing knoroot (${knoroot}) to support a prefix-build";
            rm .knoroot;
            rm -f options/knoroot;
        fi;
    fi;
}

BEARHUG=
if which bear > /dev/null 2>&1; then
    BEARHUG="bear --output x_compile_commands.json -- ";
fi

# Start handling arugments

# These are all direct commands, of which there should be just one
case $1 in
    # First (command) arg
    activate)
        # Activates a knoroot, emitting code for setting
        # appropriate environment variables
        if [ -f .knoroot ]; then
            echo "# knob: Using default $(cat .knoroot)" >&2;
            exec ./bin/useimg;
            exit;
        else
            echo "knob: no specified or default KNOROOT" >&2;
        fi;
        ;;
    # First (command) arg
    log|logs)
        shift; display_logs $@;
        exit;
        ;;
    errors|errs|err)
        shift; display_logs errs $@;
        exit;
        ;;
    help|usage)
        usage;
        exit;
        ;;
    setopt|set)
	if [ $# -lt 2 ]; then
            usage;
	    exit;
	fi;
	shift; optname=$1;
        if [ "${optname}" = "opt" ] || [ "${optname}" = "opts" ]; then
            shift; optname=$1;
        fi;
	if [ "${optname#*=}" = "${optname}" ]; then
	    shift;
	    newval="$*";
	else
	    newval="${optname#*=}";
	    optname="${optname%%=*}";
	fi;
        set_option "${optname}" "${newval}";
        exit;
	;;
    setopts)
	if [ $# -lt 2 ]; then
            echo "# Usage: knob setopts [var=val]|[var val]*";
	    exit;
	fi;
        shift;
        while [ $# -gt 0 ]; do
	    if [ "${1#*=}" = "${1}" ]; then
                optname="$1";
	        newval="$2";
                shift; shift;
            else
	        optname="${1%%=*}";
	        newval="${1#*=}";
                shift;
            fi;
            set_option "${optname}" "${newval}";
        done;
        exit;
	;;
    clearopt|unsetopt|unset)
	if [ $# -ne 2 ]; then
	    usage;
	    exit;
        else
            optname=$2;
        fi;
        if [ -f "options/${optname}" ]; then
	    echo "# knob: clearing option ${optname}=$(cat options/${optname})";
	    rm "options/${optname}";
            exit;
        fi;
        if [ "${optname}" = "opt" ] || [ "${optname}" = "opts" ]; then
            shift; optname=$1;
        fi;
        if [ -z "${optname}" ]; then
            echo "# knob: No option name specified";
            exit;
	elif [ -f "options/${optname}" ]; then
	    echo "# knob: clearing option ${optname}=$(cat options/${optname})";
	    rm "options/${optname}";
	else
	    echo "# knob: No option ${optname}";
	fi;
	exit;
	;;
    get|getopt)
	if [ $# -eq 2 ]; then
	    if [ -f "options/$2" ]; then
		cat options/$2;
                exit 0;
	    else
		echo "# knob: No option $2 specified" >&2;
                exit 1;
	    fi;
	elif [ $# -gt 1 ]; then
	    shift;
	    for optname in $@; do
		if [ -f options/${optname} ]; then
		    echo "${optname}	'$(cat options/${optname})'";
		fi;
	    done;
	else for opt in options/*; do
                 if [ "${opt}" = "options/README" ]; then
		     :;
                 elif [ -d "${opt}" ]; then
                     for subopt in "${opt}/"*; do
		         echo "${subopt#options/}	$(cat ${subopt})";
                     done;
                 elif [ -f "${opt}" ] && [ "${opt%~}" = "${opt}" ]; then
		     echo "${opt#options/}	$(cat ${opt})";
                 fi;
	     done;
	fi;
	exit;
        ;;
    info|show|option|options|opts|settings)
	if [ $# -gt 1 ]; then
	    shift;
	    for optname in $@; do
		if [ -f options/${optname} ]; then
		    echo "  ${optname}='$(cat options/${optname})'";
		else
		    echo "  No ${optname}";
		fi;
	    done;
	else for opt in options/*; do
                 if [ -f "${opt}" ]; then
                     if [ "${opt}" = "options/README" ]; then
		         :;
                     elif [ "${opt}" = "options/defaults" ]; then
		         :;
                     elif [ -d "${opt}" ]; then
                         for subopt in "${opt}/"*; do
		             echo "  ${subopt#options/}=$(cat ${subopt})";
                         done;
                     elif [ -f "${opt}" ] && [ "${opt%~}" = "${opt}" ]; then
		         echo "  ${opt#options/}=$(cat ${opt})";
                     fi;
                 fi;
	     done;
	fi;
	exit;
        ;;
    map|iter|loop|gitcheck)
        # Iterates over all the .git subdirs and executes a git command in each one
        CMD=$1;
        ITER_TEST=true;
        SHOW_HEADER=true;
        shift;
        if [ "${CMD}" = "gitcheck" ]; then
            USECMD="$(pwd)/bin/gitcheck";
        elif [ -x "$(pwd)/bin/iter.$1" ]; then
            USECMD="$(pwd)/iter.$1";
            shift;
        else
            USECMD=$1;
            shift;
        fi;
        DIRS=
        while [ $# -gt 0 ]; do
            if [ "$1" = "contribs" ]; then
                DIRS="${DIRS} $(bin/contrib_dirs)";
                shift;
            elif [ "$1" = "all" ]; then
                DIRS="${DIRS} $(bin/all_dirs)";
                shift;
            elif [ -d "$1/.git" ]; then
                if ! disabledp $1; then
                    DIRS="${DIRS} $1";
                fi;
                shift;
            elif [ -d "kno-$1/.git" ]; then
                if ! disabledp $1; then
                    DIRS="${DIRS} kno-$1";
                fi;
                shift;
            else break;
            fi;
        done
        if [ -z "${DIRS}" ]; then
	    for dir in *; do
		if [ -d "${dir}/.git" ]; then
		    DIRS="${DIRS} ${dir}";
		fi;
	    done;
        fi;
        for dir in ${DIRS}; do
            ("cd" ${dir} > /dev/null || exit;
             # Iter test can be used to filter directories from listing
             if ${ITER_TEST}; then
                 if ${SHOW_HEADER}; then
		     echo "#### ${dir} ##############################################";
		     echo "# knob: In ${dir}, " ${USECMD} $@;
		 fi;
                 ${USECMD} $@;
             fi) ||
                echo "knob: command failed in ${dir} - $*";
        done;
        exit;
        ;;
    git|mapgit|gitmap)
        # Iterates over all the .git subdirs and executes command in each one
        shift;
        ITER_TEST=true;
        SHOW_HEADER=true;
        if [ -x "$(pwd)/bin/git.$1" ]; then
            SHOW_HEADER=false;
            USECMD="$(pwd)/bin/git.$1";
            shift;
        else
            USECMD="git $1";
            shift;
        fi;
        DIRS=
        ARGS=
        while [ $# -gt 0 ]; do
            if [ "$1" = "contribs" ]; then
                DIRS="${DIRS} $(bin/contrib_dirs)";
                shift;
            elif [ "$1" = "all" ]; then
                DIRS="${DIRS} $(bin/all_dirs)";
                shift;
            elif [ -d "$1/.git" ]; then
                if ! disabledp $1; then
                    DIRS="${DIRS} $1";
                fi;
                DIRS="${DIRS} $1";
                shift;
            elif [ -d "kno-$1/.git" ]; then
                if ! disabledp $1; then
                    DIRS="${DIRS} kno-$1";
                fi;
                shift;
            elif [ "$1" = "-m" ] || [ "$1" = "--message" ]; then
                if [ $# -lt 2 ]; then
                    usage;
                    echo "# $0: git commit $1 requires an argument";
                    exit;
                fi;
                GIT_MESSAGE=$2;
                shift;
            elif [ "${1#* }" = "$1" ]; then
                ARGS="${ARGS} $1";
                shift;
            else
                ARGS="${ARGS} '$1'";
                shift;
            fi;
        done
        if [ -z "${DIRS}" ]; then
            DIRS='*';
        fi;
        #echo "USECMD='${USECMD}' ARGS='${ARGS}' DIRS='${DIRS}'";
        for dir in ${DIRS}; do
            if [ -d "${dir}/.git" ]; then
                ("cd" ${dir} > /dev/null || exit;
                 # Iter test can be used to filter directories from listing
                 if ${ITER_TEST}; then
                     if ${SHOW_HEADER}; then
			 echo "#### ${dir} ##############################################";
			 echo "# knob: In ${dir}, " ${USECMD} $@;
		     fi;
                     if [ -n "${GIT_MESSAGE}" ]; then
                         ${USECMD} -m "${GIT_MESSAGE}" ${ARGS};
                     else
                         ${USECMD} ${ARGS};
                     fi;
                 fi) ||
                    echo "knob: command failed in ${dir} - $*";
            fi;
        done;
        exit;
        ;;
    contribs|all)
        # This is a virtual component, which will be included during
        # argument processing
        ;;
    maketar|mktar|mkbintar|mkdisttar|makedist|mkdist)
        RUN_SCRIPT=./bin/maketar; shift;
        ;;
    mksrctar|makesourcetar|mksourcetar|srctar|sourcetar)
        RUN_SCRIPT=./bin/makesourcetar; shift;
        DONE=false;
        while ! ${DONE}; do
            case $1 in
                --git)
                    export TARTYPE=git;
                    shift;
                    ;;
                --*)
                    export TARTYPE=git;
                    if [ -z "${GITFLAGS}" ]; then
                        export GITFLAGS="$1";
                    else
                        export GITFLAGS="${GITFLAGS} $1";
                    fi;
                    shift;
                    ;;
                *)
                    break;
                    ;;
            esac;
        done;
        ;;
    *=*)
	# environment setting for later commands
	;;
    *)
        if [ -x "bin/$1" ]; then
            RUN_SCRIPT=./bin/$1; shift;
	fi;
	;;
esac;

# These are all build/command modifiers
while [ $# -gt 0 ]; do
    case $1 in
    verbose|-v|--verbose|-verbose)
        VERBOSE_KNOBUILD=true;
        shift;
        ;;
    debug|--debug|-d|-debug)
        DEBUG_KNOBUILD=true;
        VERBOSE_KNOBUILD=true;
        shift;
        ;;
    trace|--trace|-trace)
        DEBUG_KNOBUILD=yes;
        VERBOSE_KNOBUILD=yes;
        set -x
        shift;
        ;;
    noinstall|skipinstall|+skipinstall|-install)
        SKIP_INSTALL=true;
        shift;
        ;;
    notests|skiptests|skiptest|notest|+skiptests|-tests)
        SKIP_TESTS=true;
        shift;
        ;;
    rebuild|+rebuild)
        export REBUILD=true;
        shift;
        ;;
    +reconfigure|+reconfig)
        RECONFIGURE=true;
        shift;
        ;;
    configure|config)
        SKIP_MAKE=true;
        SKIP_FASLS=true;
        SKIP_TESTS=true;
        SKIP_INSTALL=true;
        shift;
        ;;
    install)
        SKIP_CONFIG=true;
        SKIP_MAKE=true;
        SKIP_FASLS=true;
        SKIP_TESTS=true;
        shift;
        ;;
    build)
        SKIP_CONFIG=false;
        SKIP_MAKE=false;
        SKIP_FASLS=false;
        SKIP_TESTS=false;
        SKIP_INSTALL=false;
        shift;
        ;;
    compile)
        SKIP_FASLS=true;
        SKIP_TESTS=true;
        SKIP_INSTALL=true;
        shift;
        ;;
    make|justmake)
        SKIP_TESTS=true;
        SKIP_INSTALL=true;
        shift;
        ;;
    mkfasl|mkfasls|fasl|fasls|faslize)
        SKIP_CONFIG=true;
        SKIP_FASLS=false;
        SKIP_TESTS=true;
        SKIP_INSTALL=true;
        shift;
        ;;
    test|runtests)
        SKIP_CONFIG=true;
        SKIP_MAKE=true;
        SKIP_FASLS=true;
        SKIP_INSTALL=true;
        MAKE_FLAGS="FORCE_TESTS=true";
        shift;
        ;;
    reconfigure|reconfig)
        RECONFIGURE=true;
        SKIP_MAKE=true;
        SKIP_FASLS=true;
        SKIP_TESTS=true;
        SKIP_INSTALL=true;
        shift;
        ;;
    testbuild|buildtest)
        SKIP_INSTALL=true;
        SKIP_FASLS=true;
        shift;
        ;;
    compile|recompile|justbuild|devbuild)
        SKIP_INSTALL=true;
        SKIP_TESTS=true;
        shift;
        ;;
    freshbuild|fresh)
        SKIP_INSTALL=true;
        SKIP_TESTS=true;
        SKIP_FASLS=true;
        REBUILD=true;
        shift;
        ;;
    savetests|savetest|+savetests|+savetest|+logtest|+logtests)
        SAVE_TESTS=true;
        shift;
        ;;
    skipfasls|skipfasl|skipfaslize|nofasl|nofasls|+skipfasl|+skipfasls|-fasls)
        SKIP_FASLS=true;
        shift;
        ;;
    ignoretests|+ignoretests)
        export IGNORE_TESTS=true;
        shift;
        ;;
    components|list|showall)
        SHOW_COMPONENTS=yes;
        shift;
        ;;
    SAVEIMAGE*)
        export SAVEIMAGE=${1#SAVEIMAGE=};
        shift;
        ;;
    *=*)
	setopt=$1; shift;
        optname=${setopt%%=*};
        optval=${setopt#*=};
        if [ -z "${optval}" ]; then
            rm -f "./options/${optname}";
        else
            echo "${optval}" > "./options/${optname}";
        fi;
        case ${optname} in
            knoroot)
                if [ ! -d "${newval}" ]; then mkdir "${newval}"; fi;
                KNOROOT=${useval};
                ;;
            branch)
                if [ -z "${useval}" ]; then
                    unset BRANCH;
                else
                    BRANCH=${useval};
                    USEBRANCH=${useval};
                fi;
                ;;
        esac;
        ;;
    *)
        break;
        ;;
    esac;
done;


. ./common.sh

# Get build modifiers from options

if testbuildopt savetests; then
    SAVE_TESTS=true;
fi;

if testbuildopt devmode; then
    MAKE_FLAGS="DEVMODE=true ${MAKE_FLAGS}";
fi;

if testbuildopt installmods; then
    INSTALL_MODS="$(getbuildopt installmods)";
fi;

if testbuildopt reckless || testbuildopt ignoretests; then
    export IGNORE_TESTS=true;
fi;

dbgmsg "Scanning $# arguments for components and modifiers" $@;

# Collect component names, make-targets, config args, and environment bindings
for arg in $@; do
    if [ -d "${arg}" ]; then
        COMPONENTS="${COMPONENTS} ${arg}";
    elif [ -d "kno-${arg}" ]; then
        COMPONENTS="${COMPONENTS} kno-${arg}";
    elif known_componentp ${arg}; then
        COMPONENTS="${COMPONENTS} ${arg}";
    elif [ "${arg}" = "contribs" ] || [ "${arg}" = "contrib" ]; then
        for contrib in ${USE_CONTRIBS}; do
            COMPONENTS="${COMPONENTS} kno-${contrib}";
        done;
    elif [ "${arg}" = "all" ]; then
        COMPONENTS=${KNO_COMPONENTS};
    elif [ -d "DIY/${arg}" ]; then
        COMPONENTS="${COMPONENTS} DIY/${arg}";
    elif [ -x "bin/build.${arg}" ]; then
        COMPONENTS="${COMPONENTS} ${arg}";
    elif [ "${arg#-}" != "${arg}" ]; then
        CONFIG_ARGS="${CONFIG_ARGS} ${arg}";
    elif [ "${arg#*=}" != "${arg}" ]; then
        ENV="${ENV} ${arg}";
    elif [ -z "${MAKE_TARGETS}" ]; then
        MAKE_TARGETS="${arg}";
    else case ${arg} in
             skipinstall)
                 SKIP_INSTALL=true;
                 ;;
             *)
                 MAKE_TARGETS="${MAKE_TARGETS} ${arg}";
                 ;;
         esac;
    fi;
done;

# Strip any leading ot trailing whitespace from components
#  which is defined initially and used to simplfy identification
#  of redundant conmponents
COMPONENTS=${COMPONENTS# };
COMPONENTS=${COMPONENTS% };

# Report underlying scripts identified
if ${VERBOSE_KNOBUILD} || ${DEBUG_KNOBUILD}; then
    if [ -n "${RUN_SCRIPT}" ]; then
        ${ECHO} "# knobuild(verbose): RUN_SCRIPT='${RUN_SCRIPT}'";
    fi;
    if [ -n "${CMD_SCRIPT}" ]; then
        ${ECHO} "# knobuild(verbose): CMD_SCRIPT='${CMD_SCRIPT}'";
    fi;
fi;
# Report gathered configuration info
if ${DEBUG_KNOBUILD}; then
    ${ECHO} "# knobuild(debug): pid=$$";
    ${ECHO} "# knobuild(debug): KNOROOT='${KNOROOT}' PREFIX='${PREFIX}'";
    ${ECHO} "# knobuild(debug): COMPONENTS='${COMPONENTS}' CONFIG_ARGS='${CONFIG_ARGS}'";
    ${ECHO} "# knobuild(debug): RECONFIGURE='${RECONFIGURE}' SKIP_CONFIG='${SKIP_CONFIG}'";
    ${ECHO} "# knobuild(debug): SKIP_MAKE='${SKIP_MAKE}' SKIP_INSTALL='${SKIP_INSTALL}'";
    ${ECHO} "# knobuild(debug): SKIP_TESTS='${SKIP_TESTS}' IGNORE_TESTS='${IGNORE_TESTS}'";
    ${ECHO} "# knobuild(debug): MAKE_TARGETS='${MAKE_TARGETS}'";
    ${ECHO} "# knobuild(debug): MAKE_FLAGS: ${MAKE_FLAGS}"
fi;

if [ -n "${BUILDMODE}" ]; then
    case ${BUILDMODE} in
        debugging|debug)
            MAKE_TARGETS=debug;
            ;;
        normal)
            ;;
        *)
            echo "Unknown buildmode '${BUILDMODE}' for kno";
            ;;
    esac;
fi;

NOTES=

if [ -n "${RUN_SCRIPT}" ]; then
    ${RUN_SCRIPT} $@;
    if [ -n "${BEARHUG}" ]; then bin/update_cc.json; fi;
    exit;
fi;

handle_component() {
    local component=$1;
    local cname=$2;
    if testbuildopt "${cname}.locked"; then
        echo "# $0: The knobuild component ${cname} is locked!">&2;
        return;
    fi;
    local branch=$(getbuildopt "${cname}.branch" "${BRANCH}");
    local commit=$(getbuildopt "${cname}.commit");
    local showbranch="";
    local ignore_tests=false;
    local install_mods=$(getbuildopt "${cname}.installmods" "base_modules");
    local env_args=$(get_env_args "${component}");
    INSTALL_FLAGS="INSTALLMODS=${install_mods}";

    dbgmsg "Handling component ${cname} (${component}) branch='${branch}' commit='${commit}'";

    if [ -n "${commit}" ]; then
        showbranch="#${commit}";
    elif [ -n "${USEBRANCH}" ]; then
        showbranch="#${branch}";
    fi;
    if ${IGNORE_TESTS} || [ -f "options/${cname}.ignoretests" ]; then ignore_tests=true; fi
    ('cd' "${component}" > /dev/null || exit;
     if ! gitcheckbranch; then return 1; fi;
     dbgmsg "Building ${cname}${showbranch} in $(pwd)";
     ${ECHO_INLINE} -n "# knob: Building component ${cname}...";
     if ${REBUILD}; then
         clean_phase "${component}";
     fi;
     if ${SKIP_CONFIG} && [ -f makefile ]; then
         :;
     elif ${RECONFIGURE} && [ -x configure ]; then
         dbgmsg ${ENV} ./configure --prefix="${PREFIX}" ${env_args} ${CONFIG_ARGS};
         phase_start "configure" "${cname}";
         #${ECHO_INLINE} " (configure...";
         ( ${ENV} ./configure --prefix="${PREFIX}" ${env_args} ${CONFIG_ARGS} > \
                  "../logs/${cname}.config.log" \
                  2> "../logs/${cname}.config.err" &&
               ( if [ -s "../logs/${cname}.config.err" ]; then
                     ${ECHO_INLINE} "with errors)";
                 else phase_complete configure "${cname}";
                 fi; ) ) ||
             ( phase_failed configure "${cname}" );
     elif [ -f makefile ] && [ -f config.log ] &&
              ! find . ! -readable -prune -o -newer config.log -name '*.in' | read -r 2> /dev/null; then
         phase_complete "config up to date" ${cname};
     elif [ -x config.status ] && [ -z "${CONFIG_ARGS}" ]; then
         phase_start configure "${cname}" "config refresh";
         #${ECHO_INLINE} " (config refresh...";
         (  ${ENV} ./config.status >> "../logs/${cname}.config.log" \
                   2> "../logs/${cname}.config.err" &&
                if [ -s "../logs/${cname}.config.err" ]; then
                    ${ECHO_INLINE} "with errors)";
                else phase_complete config.status "${cname}";
                fi; ) ||
             phase_failed "reconfigure" "${cname}";
     elif [ -x configure ]; then
         dbgmsg ${ENV} ./configure --prefix="${PREFIX}" ${env_args} ${CONFIG_ARGS};
         phase_start configure "${cname}";
         (  ${ENV} ./configure --prefix="${PREFIX}" ${env_args} ${CONFIG_ARGS} > \
                      "../logs/${cname}.config.log" \
                   2> "../logs/${cname}.config.err" &&
                if [ -s "../logs/${cname}.config.err" ]; then
                    ${ECHO_INLINE} "with errors)";
                else phase_complete configure "${cname}" "d)"; fi; ) ||
             phase_failed configure "${cname}";
     elif [ -f makefile ] && grep "^prep:" makefile 1> /dev/null 2>&1; then
         phase_start prepare ${cname};
         ( ${ENV} make prep > "../logs/${cname}.prep.log" 2> "../logs/${cname}.prep.err" &&
               phase_complete prepare "${cname}" "d" ) ||
             phase_failed prepare "${cname}";
     fi;
     if ${SKIP_MAKE}; then
         :;
     elif [ ! -f makefile ]; then
         echo " (no makefile)";
     else
         dbgmsg ${ENV} ${BEARHUG} make ${env_args} ${MAKE_FLAGS} ${MAKE_TARGETS};
         phase_start make ${cname}
         ( echo "#"${ENV} make ${env_args} ${MAKE_FLAGS} ${MAKE_TARGETS} > "../logs/${cname}.make.log" &&
               ${ENV} make ${env_args} ${MAKE_FLAGS} ${MAKE_TARGETS} >> "../logs/${cname}.make.log" \
                      2> "../logs/${cname}.make.err" &&
               phase_complete make "${cname}" "de") ||
             phase_failed make "${cname}";
     fi;
     if [ ! -d scheme ]; then
         :;
     elif ${SKIP_FASLS}; then
         ${ECHO_INLINE} " (skipping fasls)";
     else
         phase_start fasls "${cname}";
         if [ -f makefile ] && grep "fasl:" makefile >/dev/null 2>&1; then
             if make fasl  >> "../logs/${cname}.fasls.log" \
                     2>> "../logs/${cname}.fasls.err"; then
                 phase_complete fasls "${cname}";
             elif ! ${REQUIRE_FASLS}; then
                 phase_complete fasls "${cname}";
             else
                 phase_failed fasls "${cname}";
             fi;
         elif [ ! -d ./scheme ]; then
             :;
         elif knox mkfasl ./scheme DLOADPATH=. LOADPATH=./scheme >> "../logs/${cname}.fasls.log"\
                   2>> "../logs/${cname}.fasls.err"; then
             phase_complete fasls "${cname}";
         elif ! ${REQUIRE_FASLS}; then
             phase_complete fasls "${cname}" incomplete;
         else
             phase_failed fasls "${cname}";
         fi;
     fi;

     if [ ! -f makefile ]; then
         :;
     elif ${SKIP_TESTS} || testbuildopt ${cname}.skiptests; then
         phase_start tests ${cname};
         phase_complete tests "${cname}" " skipped";
     elif ! grep "^tests:" makefile > /dev/null 2>&1; then
         ${ECHO_INLINE} " (no tests)";
     else
         if ${SAVE_TESTS} || testbuildopt ${cname}.savetests; then
             TEST_FLAGS="SAVETESTS=true SAVE=true";
         fi;
         dbgmsg ${ENV} make ${MAKE_FLAGS} ${TEST_FLAGS} tests;
         phase_start tests ${cname};
         ( ${ENV} make ${MAKE_FLAGS} ${TEST_FLAGS} tests > "../logs/${cname}.tests.log" \
                  2> "../logs/${cname}.tests.err" &&
               if [ -f "${TEST_TARGET}.skipped" ]; then
                   phase_complete tests  "${cname}" " skipped";
               else
                   phase_complete tests  "${cname}" "ed";
               fi ) ||
             if ${ignore_tests}; then
                 ${ECHO_INLINE} "ignoring failure)";
             else
                 phase_failed tests "${cname}";
             fi;
     fi;

     if ${SKIP_INSTALL} || testbuildopt ${cname}.skipinstall; then
         phase_start install ${cname};
         phase_complete install "${cname}" " skipped";
     else
         dbgmsg ${ENV} make ${MAKE_FLAGS} INSTALL_SCOPE=base install;
         phase_start install ${cname};
         ( echo ${ENV} make ${MAKE_FLAGS} INSTALL_SCOPE=base install > "../logs/${cname}.install.log" &&
               ${ENV} make ${MAKE_FLAGS} INSTALL_SCOPE=base install >> "../logs/${cname}.install.log" \
                      2> "../logs/${cname}.install.err" &&
               phase_complete install "${cname}" "ed" ) ||
             phase_failed install "${cname}";
     fi;
     build_done;
    )
}

if [ -n "${SHOW_COMPONENTS}" ]; then
    echo "# knob: Available components are:" >&2;
    for component in ${KNO_COMPONENTS}; do
        echo "#	${component}";
    done;
    exit;
elif [ -z "${COMPONENTS}" ]; then
    echo "# knob: No components were specified";
    exit;
else for component in ${COMPONENTS}; do
         BUILD_STARTED=$(date +%s);
         cname="${component#kno-}";
         cname="${cname#DIY/}";
         if [ -f ./abort_knobuild ]; then
             if [ "$(cat ./abort_knobuild)" = "${cname}" ]; then
                 rm ./abort_knobuild;
             else
                 echo "# knob: Aborting remainder of components because" "$(cat ./abort_knobuild)";
                 break;
             fi;
         fi;
         if [ -f "./options/locked" ]; then
             echo "# $0: This knobuild installation is locked">&2;
             continue;
         fi;
         if disabledp "${cname}"; then
             echo "# knob: skipping disabled component ${component}";
             continue;
         fi;
         echo "${component}" > .component;
         if [ -x "./bin/build.${component}" ]; then
             dbgmsg "Using build script ./bin/build.${cname}";
             ${ENV} "./bin/build.${cname}";
             continue;
         elif [ -x "./bin/build.${cname}" ]; then
             dbgmsg "Using build script ./bin/build.${cname}";
             ${ENV} "./bin/build.${cname}";
             continue;
         elif [ -x "${CMD_SCRIPT}" ]; then
             ${CMD_SCRIPT} ${component};
             continue;
         fi;
         if [ -d "${component}" ]; then
             handle_component ${component} ${cname};
         else
             echo "#!! knob: No source directory directory ${component}";
         fi;
     done;
     if [ -n "${BEARHUG}" ]; then bin/update_cc.json; fi;
     if [ -f ./abort_knobuild ]; then
         echo "# knob: Aborting remainder of build because" "$(cat ./abort_knobuild)";
         # We use a file to communicate from the subprocesses, to end the 'top level' knobuild,
         # but we then delete the file because we want to be able to start again
         rm ./abort_knobuild;
     elif testbuildopt saveimage; then
         saveto=$(getbuildopt saveimage);
         case ${saveto} in
             *.tar|*.tar.gz|*.tar.bz2|*.tar.xz)
                 ./bin/maketar ${saveto};
                 ;;
             *)
                 ./bin/maketar;
                 ;;
         esac;
     elif [ -n "${SAVEIMAGE}" ]; then
         saveto="${SAVEIMAGE}";
         case ${saveto} in
             *.tar|*.tar.gz|*.tar.bz2|*.tar.xz)
                 ./bin/maketar ${saveto};
                 ;;
             *)
                 ./bin/maketar;
                 ;;
         esac;
     fi;
fi;

